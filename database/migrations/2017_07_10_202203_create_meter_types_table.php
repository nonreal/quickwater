<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMeterTypesTable extends Migration {

	public function up()
	{
		Schema::create('meter_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code', 4)->index();
			$table->string('name');
			$table->string('route')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('meter_types');
	}
}