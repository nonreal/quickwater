<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxCodesTable extends Migration {

	public function up()
	{
		Schema::create('tax_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code', 10)->index();
			$table->string('name');
			$table->float('rate')->nullable()->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('tax_codes');
	}
}