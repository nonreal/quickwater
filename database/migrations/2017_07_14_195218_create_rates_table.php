<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration {

	public function up()
	{
		Schema::create('rates', function(Blueprint $table) {
			$table->increments('id');

			$table->string('code', 4)->index();
			$table->string('name');
			$table->integer('client_id')->unsigned();
			$table->integer('meter_type_id')->unsigned();

			$table->timestamps();
		});

		Schema::table('rates', function (Blueprint $table) {
			$table->foreign('meter_type_id')->references('id')->on('meter_types')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

		Schema::table('rates', function (Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

	}

	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::drop('rates');
		Schema::enableForeignKeyConstraints();
	}
}