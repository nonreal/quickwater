<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateTaxCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_tax_code', function (Blueprint $table) {

	        $table->integer('rate_id')->unsigned()->index();
	        $table->foreign('rate_id')->references('id')->on('rates')->onDelete('cascade');

	        $table->integer('tax_code_id')->unsigned()->index();
	        $table->foreign('tax_code_id')->references('id')->on('tax_codes')->onDelete('cascade');


	        $table->smallInteger('usage')->unsigned()->nullable();
	        $table->smallInteger('pbc')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
	    Schema::dropIfExists('rate_tax_code');
	    Schema::enableForeignKeyConstraints();
    }
}
