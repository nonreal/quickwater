<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoutesTable extends Migration {

	public function up()
	{
		Schema::create('routes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code', 4)->index();
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('routes');
	}
}