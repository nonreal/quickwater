<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingPeriodUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_period_units', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name')->unique();
	        $table->string('display_name');
        });

	    DB::table('billing_period_units')->insert([

	    	['name' => 'monthly', 'display_name'=>'Monthly'],
	    	['name' => 'bi-monthly', 'display_name'=>'Bi Monthly'],
	    	['name' => 'quarterly', 'display_name'=>'Quarterly'],
	    	['name' => 'bi-annual', 'display_name'=>'Bi-Annual'],
	    	['name' => 'yearly', 'display_name'=>'Yearly'],

	    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_period_units');
    }
}
