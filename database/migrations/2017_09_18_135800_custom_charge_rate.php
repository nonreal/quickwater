<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomChargeRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('custom_charge_rate', function (Blueprint $table) {

		    $table->integer('custom_charge_id')->unsigned()->index();
		    $table->foreign('custom_charge_id')->references('id')->on('custom_charges')->onDelete('cascade');

		    $table->integer('rate_id')->unsigned()->index();
		    $table->foreign('rate_id')->references('id')->on('rates')->onDelete('cascade');

		    $table->timestamps();

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('custom_charge_rate');
        Schema::enableForeignKeyConstraints();
    }
}
