<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeterHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meter_histories', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('meter_id')->unsigned()->index();
	        $table->integer('customer_id')->unsigned()->index();
	        $table->string('action', 128);

            $table->timestamps();
        });

	    Schema::table('meter_histories', function (Blueprint $table) {
		    $table->foreign('customer_id')->references('id')->on('customers')
			    ->onDelete('cascade')
			    ->onUpdate('cascade');
	    });

	    Schema::table('meter_histories', function (Blueprint $table) {
		    $table->foreign('meter_id')->references('id')->on('meters')
			    ->onDelete('cascade')
			    ->onUpdate('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
    	Schema::dropIfExists('meter_histories');
    	Schema::enableForeignKeyConstraints();
    }
}
