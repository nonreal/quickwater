<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMetersTable extends Migration {

	public function up()
	{

		Schema::create('meters', function(Blueprint $table) {

			$table->increments('id');

			$table->integer('meter_type_id')->unsigned()->index()->nullable();
			$table->integer('route_id')->unsigned()->index()->nullable();
			$table->integer('zone_id')->unsigned()->index()->nullable();
			$table->integer('rate_id')->unsigned()->index()->nullable();
			$table->integer('customer_id')->unsigned()->index()->nullable();
			$table->integer('meter_unit_id')->unsigned()->index()->nullable();
			$table->integer('billing_period_id')->unsigned()->nullable();

			$table->date('billing_start');
			$table->date('billing_end')->nullable();
			$table->float('service_charge_rate', 8, 4)->unsigned()->nullable();
			$table->integer('lot_no')->unsigned()->nullable();
			$table->integer('serial')->unsigned()->nullable();
			$table->date('installed');
			$table->string('meter_miu', 10);
			$table->string('location', 32);
			$table->integer('multiplier')->unsigned();
			$table->char('read_type', 1);
			$table->enum('source', ['0', '1', '2', '3', '4', '5']);


			$table->timestamps();

		});

		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('route_id')->references('id')->on('routes')
				->onDelete('set null')
				->onUpdate('set null');
		});
		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('zone_id')->references('id')->on('zones')
				->onDelete('set null')
				->onUpdate('set null');
		});
		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('rate_id')->references('id')->on('rates')
				->onDelete('set null')
				->onUpdate('set null');
		});
		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('customer_id')->references('id')->on('customers')
				->onDelete('set null')
				->onUpdate('set null');
		});
		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('meter_unit_id')->references('id')->on('meter_units')
				->onDelete('no action')
				->onUpdate('no action');
		});

		Schema::table('meters', function (Blueprint $table) {
			$table->foreign('meter_type_id')->references('id')->on('meter_types')
				->onDelete('set null')
				->onUpdate('set null');
		});

	}

	public function down()
	{

		Schema::table('meters', function(Blueprint $table){

			$table->dropForeign('meter_type_id');
			$table->dropForeign('route_id');
			$table->dropForeign('zone_id');
			$table->dropForeign('rate_id');
			$table->dropForeign('customer_id');
			$table->dropForeign('meter_unit_id');

		});


		Schema::disableForeignKeyConstraints();
		Schema::drop('meters');
		Schema::enableForeignKeyConstraints();
	}
}