<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsageRatesTable extends Migration {

	public function up()
	{
		Schema::create('usage_rates', function(Blueprint $table) {
			$table->increments('id');

			$table->integer('rate_id')->unsigned()->index();
			$table->float('usage_rate', 10,6);
			$table->integer('break_point')->unsigned();
			$table->integer('progressive_base_charge')->unsigned();

			$table->timestamps();

			$table->foreign('rate_id')->references('id')->on('rates')
				->onDelete('cascade')
				->onUpdate('cascade');

		});
	}

	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::drop('usage_rates');
		Schema::enableForeignKeyConstraints();
	}
}