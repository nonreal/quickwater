<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeterIdToMeters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meters', function (Blueprint $table) {
	        $table->string('meter_id', 128)->after('meter_miu')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meters', function (Blueprint $table) {

	        if(Schema::hasColumn('meters', 'meter_id')){

		        $table->dropColumn('meter_id');
		        $table->dropUnique('meter_id');
	        }

        });
    }
}
