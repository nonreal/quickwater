<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_periods', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('billing_period_unit_id')->unsigned()->index();
	        $table->string('code', 32)->index();
	        $table->string('name');
	        $table->integer('date');

            $table->timestamps();
        });

	    Schema::table('meters', function (Blueprint $table) {
		    $table->foreign('billing_period_id')->references('id')->on('billing_periods')
			    ->onDelete('set null')
			    ->onUpdate('set null');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('billing_periods');

    }
}
