<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReadingsTable extends Migration {

	public function up()
	{
		Schema::create('readings', function(Blueprint $table) {
			$table->increments('id');

			$table->integer('meter_id')->unsigned()->index();
			$table->float('reading');
			$table->timestamp('date');

			$table->timestamps();
		});


		Schema::table('readings', function (Blueprint $table) {
			$table->foreign('meter_id')->references('id')->on('meters')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

	}

	public function down()
	{
		Schema::drop('readings');
	}
}