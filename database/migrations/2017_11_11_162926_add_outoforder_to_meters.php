<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOutoforderToMeters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meters', function (Blueprint $table) {
            //
	        $table->smallInteger('outoforder')->after('source')->unsigned()->default(0);
	        $table->integer('meter_digits')->after('outoforder')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meters', function (Blueprint $table) {
            //
	        $table->dropColumn('outoforder');
	        $table->dropColumn('meter_digits');
        });
    }
}
