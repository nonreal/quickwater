<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');


	        $table->string('company_name', 128);
	        $table->string('first_name', 64);
	        $table->string('last_name', 64);

	        $table->float('service_charge_rate', 8, 4)->unsigned();
	        $table->float('late_fee_amount')->unsigned();
	        $table->float('minimum_balance')->unsigned();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
