<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_charges', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('code', 32)->index();
	        $table->string('name');
	        $table->float('value', 8, 4)->unsigned()->nullable();
	        $table->integer('omit_if_zero_usage')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_charges');
    }
}
