<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('client_id')->unsigned();


	        $table->string('first_name', 64);
	        $table->string('last_name', 64);

	        $table->string('phone');
	        $table->string('phone_extra')->nullable();
	        $table->string('mobile_phone')->nullable();
	        $table->string('mobile_phone_extra')->nullable();
	        $table->string('email_extra')->nullable();


	        $table->foreign('client_id')
		        ->references('id')->on('clients')
		        ->onUpdate('cascade')
		        ->onDelete('cascade');

	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
