<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMeterUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    // Create table for storing meter units
	    Schema::create('meter_units', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('meter_type')->unsigned();
		    $table->string('name')->unique();
		    $table->string('display_name')->nullable();

		    $table->foreign('meter_type')->references('id')->on('meter_units')
			    ->onUpdate('cascade')
			    ->onDelete('cascade');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::disableForeignKeyConstraints();
	    Schema::drop('meter_units');
	    Schema::enableForeignKeyConstraints();
    }
}
