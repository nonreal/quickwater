<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Permission;
use App\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncatePermissionTables();

	    $config = config('permission.role_structure');
	    $userPermission = config('permission.permission_structure');
	    $mapPermission = collect(config('permission.permissions_map'));

	    foreach ($config as $key => $modules){

	    	// Create a  new role
		    $role = Role::create(['name' => $key]);

		    $this->command->info('Creating Role ' . strtoupper($key));

		    // Reading role permission modules
		    foreach ($modules as $module => $value) {
			    $permissions = explode(',', $value);


			    foreach ($permissions as $p => $perm) {
				    $permissionValue = $mapPermission->get($perm);

				    $permission = Permission::create([
					    'name' => $permissionValue . '-' . $module,
				    ]);

				    $this->command->info('Creating Permission to ' . $permissionValue . ' for ' . $module);

				    if (!$role->hasPermissionTo($permission->name)) {
					    $role->givePermissionTo($permission);
				    } else {
					    $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
				    }
			    }

		    }

	    }

        /*
        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        foreach ($config as $key => $modules) {
            // Create a new role
            $role = \Quickwater\Role::create([
                'name' => $key,
                'display_name' => ucwords(str_replace("_", " ", $key)),
                'description' => ucwords(str_replace("_", " ", $key))
            ]);

            $this->command->info('Creating Role '. strtoupper($key));

            // Reading role permission modules
            foreach ($modules as $module => $value) {
                $permissions = explode(',', $value);

                foreach ($permissions as $p => $perm) {
                    $permissionValue = $mapPermission->get($perm);

                    $permission = \Quickwater\Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ]);

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                    
                    if (!$role->hasPermission($permission->name)) {
                        $role->attachPermission($permission);
                    } else {
                        $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
                    }
                }
            }

            $this->command->info("Creating '{$key}' user");
            // Create default user for each role
            $user = \Quickwater\User::create([
                'name' => ucwords(str_replace("_", " ", $key)),
                'email' => $key.'@app.com',
                'password' => bcrypt('password')
            ]);
            $user->attachRole($role);
        }

        // creating user with permissions
        if (!empty($userPermission)) {
            foreach ($userPermission as $key => $modules) {
                foreach ($modules as $module => $value) {
                    $permissions = explode(',', $value);
                    // Create default user for each permission set
                    $user = \Quickwater\User::create([
                        'name' => ucwords(str_replace("_", " ", $key)),
                        'email' => $key.'@app.com',
                        'password' => bcrypt('password'),
                        'remember_token' => str_random(10),
                    ]);
                    foreach ($permissions as $p => $perm) {
                        $permissionValue = $mapPermission->get($perm);

                        $permission = \Quickwater\Permission::firstOrCreate([
                            'name' => $permissionValue . '-' . $module,
                            'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                            'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        ]);

                        $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                        
                        if (!$user->hasPermission($permission->name)) {
                            $user->attachPermission($permission);
                        } else {
                            $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
                        }
                    }
                }
            }
        }
        */
    }

    /**
     * Truncates all the laratrust tables and the users table
     * @return    void
     */
    public function truncatePermissionTables()
    {

    	DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    	$config = config('permission.table_names');

    	foreach($config as $key => $table){
    		// Truncate tables
		    DB::table($table)->truncate();

	    }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
