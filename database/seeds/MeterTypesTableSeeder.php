<?php

use App\MeterType;
use Illuminate\Database\Seeder;

class MeterTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

	    DB::table('meter_types')->truncate();

	    MeterType::insert(
	    	[
			    [
				    'code' => 'W',
				    'name' => 'water',
			    ],
			    [
				    'code' => 'E',
				    'name' => 'Electricity',
			    ],
			    [
				    'code' => 'G',
				    'name' => 'Gas',
			    ]
		    ]
	    );
    }
}
