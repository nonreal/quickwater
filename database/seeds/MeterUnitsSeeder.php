<?php

use App\MeterType;
use Illuminate\Database\Seeder;

class MeterUnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	    DB::table('meter_units')->truncate();

	    // Insert Water Units
	    $water_id = $mt = MeterType::where('code', 'W')->pluck('id')->first();

	    DB::table('meter_units')->insert([
		    ['meter_type' => $water_id, 'name' => 'acre-foot', 'display_name' => 'Acre Foot'],
		    ['meter_type' => $water_id, 'name' => 'cubic-feet', 'display_name' => 'Cubic Feet'],
		    ['meter_type' => $water_id, 'name' => 'cubic-meter', 'display_name' => 'Cubic Meter'],
		    ['meter_type' => $water_id, 'name' => 'gallons', 'display_name' => 'Gallons'],
	    ]);

    }
}
