@extends('adminlte::page')

@section('htmlheader_title')
    Readings
@endsection

@section('contentheader_title')
    Readings
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Readings</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">


                        <table class="table table-hover table-responsive table-striped">

                            <thead>
                            <tr>
                                <th>Meter Serial #</th>
                                <th>Name</th>
                                <th>Previous</th>
                                <th>Last Reading</th>
                                <th>New Reading</th>
                                <th>Date</th>
                                <th>Units</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($meters as $meter)

                                <tr>
                                    <td>{{ $meter->serial }}</td>
                                    <td>{{ $meter->customer->full_name }}</td>
                                    <td>
                                        @if(!$meter->readings->isEmpty())
                                            {{ $meter->readings->get(1)->reading }}
                                        @endif

                                        @if(!$meter->readingsLast(2)->get()->isEmpty())
                                            {{ $meter->readingsLast(2)->get()->reverse()->first()->reading }}
                                        @endif()
                                    </td>
                                    <td>
                                        @if(!$meter->readings->isEmpty())
                                            {{ $meter->readings->first()->reading }}
                                        @endif

                                        @if(!$meter->readingsLast(2)->get()->isEmpty())
                                            {{ $meter->readingsLast(2)->get()->reverse()->last()->reading }}
                                        @endif()
                                    </td>
                                    <td>
                                        <input type="text"/>
                                    </td>
                                    <td><input type="date"/></td>
                                    <td></td>
                                    <td>
                                        <button class="btn btn-info btn-sm pull-left margin-r-5" data-meter="{{$meter->id}}">Calculate Estimate</button>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>


                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
