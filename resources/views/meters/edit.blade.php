@push('stylesheets')
    <!-- Select2 -->
    {{--<link href="{{ asset('plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>--}}
@endpush

@extends('adminlte::page')

@section('htmlheader_title')
	| Edit Meter
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new note</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{ Form::open(array('method'=> 'POST', 'route' => ['meters-note.add', $meter])) }}
                    <div class="box-body">
                        <div class="form-group">
                            {{ Form::label('note', 'Note') }}
                            {{ Form::textarea('content', null, ['class' => 'form-control', 'rows' => 4, 'required' => true, 'placeholder'=>'Add new note to meter' ]) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Notes</h3>
                        <!-- /.box-tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding"></div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>


        {!! form_start($form) !!}

        <div class="row">
			<div class="col-md-12">

				<div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Meter</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="col-md-6">
                            {!! form_until($form, 'meter_digits') !!}
                        </div>

                        <div class="col-md-6">
                            {!! form_until($form, 'read_type') !!}
                        </div>

                        <div class="col-md-12">
                            {!! form_row($form->outoforder) !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>


        <div class="row">

            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Meter Address</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="collection-container" data-prototype="{{ form_row($form->addresses->prototype()) }}">
                            {!! form_row($form->addresses) !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-md-12">

                {!! form_row($form->submit) !!}

            </div>

        </div>
        {!! form_end($form, false) !!}
	</div>


    <div class="modal fade" id="showModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span></button>
                    <h4 class="modal-title">Note</h4>
                </div>
                <div class="modal-body">
                    <div id="note_show">first note. put something!!</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="editModal">
        <div class="modal-dialog">
            <div class="modal-content">

                {{ Form::open(array('url' => 'notes', 'method'=> 'PUT', 'id' => 'editNote')) }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('note', 'Note') }}
                        {{ Form::textarea('content', null, ['id'=>'note_edit', 'class' => 'form-control', 'required' => true, 'placeholder'=>'Add new note to customer' ]) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {{ Form::submit('Save changes', array('class' => 'btn btn-primary edit', 'id' => 'saveNote')) }}
                </div>

                {{ Form::close() }}

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection




@push('scripts')
    <script src="{{ asset('/plugins/select2/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">

      url = '@php echo route('notes') @endphp'

      function updateTable () {
        let request = $.get('@php echo route('notes.update-table', ['meters', $meter]) @endphp'); // make request
        let container = $('.table-responsive');

        request.done(function (data) { // success
          container.html(data.html);
        });
      }

      $(document).ready(function () {

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        })

        $('.select2').select2();

        updateTable();





        // Show note
        $(document).on('click', '.show-modal', function (e) {
          $('#note_show').text($(this).data('content'))
        })

        // Edit note
        $(document).on('click', '.edit-modal', function (e) {

          id = $(this).data('id')
          content = $(this).data('content')
          form = $($(this).data('target')).find('form')

          // set form content
          form.find('textarea[name=\'content\']').val(content)

          // change form action
          form.attr('action', url + '/' + id)
        })

        $(document).on('click', '#saveNote', function (e) {
          e.preventDefault()

          form = $('#editModal').find('form')
          let form_action = form.attr('action')
          let _token = $('input[name=\'_token\']').val()
          let content = form.find('textarea[name=\'content\']').val()

          /*
          $.ajax({
              dataType: 'json',
              type: 'PUT',
              url: form_action,
              data: {_token: _token, content: content},

          }).done(function (data) {
              $(".modal").modal('hide');
              toastr.success(data.success, {
                "progressBar": true,
                "positionClass": "toast-top-center",
                timeOut: 5000
              });
          });

          */

          let request = $.ajax({
            dataType: 'json',
            type: 'PUT',
            url: form_action,
            data: {_token: _token, content: content},
          })

          request.done(function (data) {

            $('.modal').modal('hide')
            console.log(data)
            updateTable()

            if ($.isEmptyObject(data.error)) {

              toastr.success(data.success, '', {
                positionClass: 'toast-top-full-width',
                timeOut: 5000
              })

            } else {

              toastr.error(data.error, '', {
                positionClass: 'toast-top-full-width',
                timeOut: 5000
              })
            }

          })

          request.fail(function (jqXHR, textStatus) {
            alert('Request failed: ' + textStatus)
          })

        })

      })
    </script>
@endpush
