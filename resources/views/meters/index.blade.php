@extends('adminlte::page')

@section('htmlheader_title')
	| Meters
@endsection

@section('contentheader_title')
    Meters
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Meters</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="row">
                            <div class="col-sm-12">
                                Filter:
                                <a href="{{route('meters.index')}}" class="btn btn-default btn-sm margin-r-5">All</a>
                                <a href="{{route('meters.index')}}/?outoforder" class="btn btn-default btn-sm margin-r-5">Out of order</a>
                                <a href="{{route('meters.index')}}/?no_customer" class="btn btn-default btn-sm margin-r-5">No customer</a>
                            </div>
                        </div>
                        <div class="row table-responsive no-padding">
                            <div class="col-sm-12 ">
                                <table class="table table-hover table-responsive">

                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Customer</th>
                                        <th>Parent</th>
                                        <th>Serial</th>
                                        <th>Route</th>
                                        <th>Zone</th>
                                        <th>Rate</th>
                                        <th>Units</th>
                                        <th>Billing Period</th>
                                        <th>Out of Order</th>
                                        <th>Notes</th>
                                        <th>Operations</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($meters as $row)

                                        @if($row->customer_id)
                                            @php
                                                $row_class = '';
                                                $show_customer = true;
                                            @endphp
                                        @else
                                            @php
                                                $row_class = 'bg-danger';
                                                $show_customer  = false;
                                            @endphp
                                        @endif

                                        <tr class="{{$row_class}}">
                                            <td>{{$row->id}}</td>
                                            <td>
                                                @if($show_customer)
                                                    <a href="{{ route('customers.show', ['id' => $row->customer->id], false) }}">
                                                        {{$row->customer->full_name}}
                                                        <span class="label label-success">View</span>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>{{ $row->getParent() }}</td>
                                            <td>{{$row->serial}}</td>
                                            <td>{{ $row->route->name }}</td>
                                            <td>{{ $row->zone->name }}</td>
                                            <td>{{$row->rate->name}}</td>
                                            <td>{{$row->meterUnit->display_name}}</td>
                                            <td>{{$row->billingPeriod->name}}</td>
                                            <td>{{ $row->outoforder }}</td>
                                            <td>{{ $row->notes->count() }}</td>
                                            <td>
                                                <a href="{{ route('meters.edit', ['id'=>$row->id]) }}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>

                                                {!! Form::open(['method' => 'DELETE', 'class'=>'inline', 'route' => ['meters.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                                {!! Form::close() !!}

                                                @if($show_customer)
                                                    {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.remove-customer', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to remove  from customer ?")' ]) !!}
                                                    {!! Form::submit('Remove from Customer', ['class' => 'btn btn-danger btn-sm']) !!}
                                                    {!! Form::close() !!}
                                                @endif

                                                @if($row->outoforder == '0')
                                                    {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.outoforder', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to set out of order ?")' ]) !!}
                                                    {!! Form::submit('Set out of order', ['class' => 'btn btn-danger btn-sm']) !!}
                                                    {!! Form::close() !!}
                                                @endif

                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>


                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                {{ $meters->links() }}
                            </div>
                        </div>



                    </div>

                    <!-- /.box-body -->
                </div>


			</div>
		</div>

	</div>
@endsection
