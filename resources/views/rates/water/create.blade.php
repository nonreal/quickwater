@extends('adminlte::page')

@section('htmlheader_title')
    Rates | Add
@endsection

@section('contentheader_title')
    Add Rate
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Add Rate</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    {!! form_start($form) !!}
                    <div class="box-body">

                        {!! form_row($form->code) !!}
                        {!! form_row($form->name) !!}
                        {{--{!! form_row($form->customCharges) !!}--}}


                        <fieldset class="form-group">
                            <legend>Usage Table</legend>

                            <div class="collection-container" data-prototype="{{ form_row($form->usageRates->prototype()) }}">
                                {!! form_row($form->usageRates) !!}
                            </div>
                        </fieldset>

                        <button type="button" class="add-to-collection btn btn-info pull-right">Add Usage Rate</button>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {!! form_row($form->submit) !!}
                    </div>

                    {!! form_end($form, false) !!}

                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script>
  $(document).ready(function () {
    $(`.add-to-collection`).on('click', function (e) {
      e.preventDefault()
      var container = $('.collection-container')
      var appendTo = $('.usage-rates')
      var count = appendTo.children().length
      var proto = container.data('prototype').replace(/__NAME__/g, count)
//            container.append(proto);
      appendTo.append(proto)

    })
  })
</script>

@endpush
