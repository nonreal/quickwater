@extends('adminlte::page')

@section('htmlheader_title')
	Rates | Edit
@endsection

@section('contentheader_title')
    Edit Rate
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9">

				<div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Rate</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->


                    {!! form_start($form) !!}
                    <div class="box-body">

                        {!! form_row($form->code) !!}
                        {!! form_row($form->name) !!}


                        <fieldset class="form-group">
                            <legend>Usage Table</legend>

                            <div class="collection-container" data-prototype="{{ form_row($form->usageRates->prototype()) }}">
                                {!! form_row($form->usageRates) !!}
                            </div>
                        </fieldset>

                        <button type="button" class="add-to-collection btn btn-info pull-right">Add Usage Rate</button>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {!! form_row($form->submit) !!}
                    </div>

                    {!! form_end($form, false) !!}

                </div>

			</div>
		</div>
	</div>
@endsection
