@extends('adminlte::page')

@section('htmlheader_title')
    Custom Charges | Add
@endsection

@section('contentheader_title')
    Add Custom Charge
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Add Custom Charge</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {{ Form::open(array('url' => 'custom-charge')) }}

                        <div class="form-group">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code', null, ['class' => 'form-control', 'required' => true, 'maxlength'=>32, 'placeholder'=>'32 letters max' ]) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('value', 'Value') }}
                            {{ Form::text('value', null, ['class' => 'form-control', 'required']) }}
                        </div>
                        <div class="form-check">
                            {{ Form::checkbox('omit_if_zero_usage', 1) }}
                            {{ Form::label('omit_if_zero_usage', 'Omit if zero Usage') }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
