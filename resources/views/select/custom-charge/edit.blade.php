@extends('adminlte::page')

@section('htmlheader_title')
    Custom Charges | Edit
@endsection

@section('contentheader_title')
    Edit Custom Charge
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9">

				<div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Custom Charges</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {{ Form::model($entry, array('route' => array('custom-charge.update', $entry->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                        <div class="form-group">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code', null, array('class' => 'form-control', 'required' => true)) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required' => true] ) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Value') }}
                            {{ Form::text('value', null, ['class' => 'form-control', 'required']) }}
                        </div>

                        <div class="form-check">
                            {{ Form::checkbox('omit_if_zero_usage', 1) }}
                            {{ Form::label('omit_if_zero_usage', 'Omit if zero Usage') }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
