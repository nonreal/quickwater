@extends('adminlte::page')

@section('htmlheader_title')
	Custom Charges
@endsection

@section('contentheader_title')
    Custom Charges
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9">

				<div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Zones</h3>
                        <div class="box-tools pull-right">
                            <a href="{{ route('custom-charge.create') }}" class="btn btn-sm btn bg-orange">Add Custom Charge</a>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-responsive table-striped">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Omit if 0 Usage</th>
                                <th>Operations</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($rows as $row)

                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$row->code}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->value}}</td>
                                    <td>{{$row->omit_if_zero_usage}}</td>
                                    <td>
                                        <a href="{!! route('custom-charge.edit', ['id'=>$row->id]) !!}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['custom-charge.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>

                            @endforeach

                            </tbody>


                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
