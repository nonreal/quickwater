@extends('adminlte::page')

@section('htmlheader_title')
    Billing Period | Add
@endsection

@section('contentheader_title')
    Add Billing Period
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Add Billing Period</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {{ Form::open(array('url' => 'billing-period')) }}

                        <div class="form-group">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code', null, array('class' => 'form-control', 'required' => true)) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required' => true] ) }}
                        </div>

                        <div class="form-group col-md-1 no-padding">
                            {{ Form::label('name', 'Date') }}
                            {{ Form::selectRange('date', 1, 31, null, ['class' => 'form-control', 'required' => true] ) }}
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group">
                            {{ Form::label('name', 'Range') }}
                            {{ Form::select('billing_period_unit_id', $range, null, ['class'=>'form-control']) }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
