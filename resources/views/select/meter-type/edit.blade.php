@extends('adminlte::page')

@section('htmlheader_title')
	Meter Types | Edit
@endsection

@section('contentheader_title')
    Edit Meter Type
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9">

				<div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Meter Type</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {{ Form::model($entry, array('route' => array('meter-type.update', $entry->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                        <div class="form-group">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code', null, array('class' => 'form-control', 'required' => true)) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required' => true] ) }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
