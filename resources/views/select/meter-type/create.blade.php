@extends('adminlte::page')

@section('htmlheader_title')
    Meter Types | Add
@endsection

@section('contentheader_title')
    Add Meter Type
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Add Meter Type</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {{ Form::open(array('url' => 'meter-type')) }}

                        <div class="form-group">
                            {{ Form::label('code', 'Code') }}
                            {{ Form::text('code', null, ['class' => 'form-control', 'required' => true, 'maxlength'=>4, 'placeholder'=>'4 leters max' ]) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'required']) }}
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
