<table class="table table-hover table-responsive table-striped">

    <thead>
    <tr>
        <th width="10%">Id</th>
        <th width="70%">Note</th>
        <th>Operations</th>
        <th></th>
    </tr>
    </thead>

    <tbody>

    @foreach($rows as $row)

        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{str_limit($row->content, 150)}}</td>
            <td>
                <button class="show-modal btn btn-default btn-sm pull-left margin-r-5" data-toggle="modal" data-target="#showModal"
                        data-id="{{$row->id}}"
                        data-content="{{$row->content}}">
                    <i class="fa fa-eye"></i> View
                </button>

                <button class="edit-modal btn btn-info btn-sm pull-left margin-r-5" data-toggle="modal" data-target="#editModal"
                        data-id="{{$row->id}}"
                        data-content="{{$row->content}}">
                    <i class="fa fa-pencil"></i> Edit
                </button>

                {!! Form::open(['method' => 'DELETE', 'route' => ['notes.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                {!! Form::close() !!}

            </td>
        </tr>

    @endforeach

    </tbody>


</table>