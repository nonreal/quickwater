<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-white">
        <div class="widget-user-image">
            <img src="{{ Gravatar::get($customer->email_extra) }}" class="user-image" alt="User Image"/>
        </div>
        <!-- /.widget-user-image -->
        <h3 class="widget-user-username">
            <a href="{{ route('customers.show', ['id' => $customer->id], false) }}">
                {{ $customer->full_name }}
            </a>
        </h3>
        <h5 class="widget-user-desc">{{ $customer->user->email }}</h5>
    </div>
</div>
