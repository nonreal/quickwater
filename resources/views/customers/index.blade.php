@extends('adminlte::page')

@section('htmlheader_title')
	Customers
@endsection

@section('contentheader_title')
    Customers
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Customers</h3>

                        <div class="pull-right box-tools">
                            <a href="{{ route('customers.create') }}" class="btn btn-sm btn bg-orange">Add Customer</a>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->


                    <div class="box-body no-padding">

                        <table class="table table-hover table-responsive table-striped">

                            <thead class="thead-inverse">
                            <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Operations</th>
                            </tr>
                            </thead>

                            <tbody>


                            @foreach($customers as $customer)

                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><a href="{{ route('customers.show', ['id' => $customer->id], false) }}">
                                            {{ $customer->full_name }}
                                            <span class="label label-success">View</span>
                                        </a></td>
                                    <td>{{$customer->user->email}}</td>
                                    <td>
                                        <a href="{!! route('customers.edit', ['id'=>$customer->id]) !!}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>

                                        {!! Form::open(['method' => 'DELETE', 'route' => ['customers.destroy', $customer->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>

                            @endforeach


                            </tbody>


                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
