@extends('adminlte::page')

@section('htmlheader_title')
	| Add Customer
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

        {!! form_start($form) !!}


        <div class="row">


			<div class="col-md-6">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Customer</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->


                    <div class="box-body">

                        <div class="col-md-12">
                            {!! form_row($form->customer->first_name) !!}
                            {!! form_row($form->customer->last_name) !!}


                            {!! form_until($form->customer->getForm(), 'email_extra') !!}

                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>


            <div class="col-md-6">
                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Login Details</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="col-md-12">
                            {!! form_row($form->user) !!}
                        </div>

                    </div>

                </div>

            </div>
		</div>

        <div class="row">

            <div class="col-md-8">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Addresses</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="collection-container" data-prototype="{{ form_row($form->customer->addresses->prototype()) }}">
                            {!! form_row($form->customer->addresses) !!}
                        </div>



                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="button" class="add-to-collection btn btn-info pull-right">Add Address</button>
                    </div>
                </div>


            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                {!! form_row($form->customer->submit) !!}

            </div>

        </div>

        {!! form_end($form, false) !!}

	</div>
@endsection

@push('scripts')
<script>
  $(document).ready(function () {
    $(`.add-to-collection`).on('click', function (e) {
      e.preventDefault()
      var container = $('.collection-container')

      var appendTo = $(this).closest('.box').children('.box-body');
      console.log(appendTo);

//      var appendTo = $('.box-body')

      var count = appendTo.children().length
      var proto = container.data('prototype').replace(/__NAME__/g, count)
      appendTo.append(proto)

    })
  })
</script>
@endpush
