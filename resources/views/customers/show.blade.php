@push('stylesheets')
    <!-- DataTables -->
    <link href="{{ asset('/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@extends('adminlte::page')

@section('htmlheader_title')
	| View Customer
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

        <div class="row">

            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue">
                        <div class="widget-user-image">
                            <img src="{{ Gravatar::get($customer->email_extra) }}" class="user-image" alt="User Image"/>
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{ $customer->full_name }}</h3>
                        <h5 class="widget-user-desc">{{ $customer->user->email }}</h5>
                    </div>


                    <div class="box-body">
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>

                        <p class="text-muted">
                            @if($customer->addresses->count() > 0)
                                {!! $customer->getPrimaryAddress()->getLine() !!}
                            @endif
                        </p>
                        <hr>
                        <div>
                            <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                            <span class="pull-right text-muted small">{{ $customer->notes->last()->created_at->diffForHumans() }}</span>
                        </div>
                        <p>{{ $customer->notes->last()->content }}</p>
                        <p></p>
                        <div><a href="{!! route('customers-notes.index', ['id'=>$customer->id]) !!}">View all notes</a></div>

                        <hr>
                        <ul class="nav nav-stacked">
                            <li><a href="#"> <strong><i class="fa fa-clock-o"></i> Meters</strong> <span class="pull-right badge bg-aqua">{{ $customer->meters->count() }}</span></a></li>
                        </ul>

                    </div>
                </div>
                <!-- /.widget-user -->
            </div>

            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add note</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{ Form::open(array('method'=> 'POST', 'route' => ['customers-note.add', $customer])) }}
                    <div class="box-body">
                        <div class="form-group">
                            {{ Form::label('note', 'Note') }}
                            {{ Form::textarea('content', null, ['class' => 'form-control', 'rows' => 4, 'required' => true, 'placeholder'=>'Add new note to customer' ]) }}
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <?php /* ?>
            <div class="col-md-4">

                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="ion ion-ios-speedometer"></i></span>

                    <div class="info-box-content">
                        <div class="box-body">

                            {{ Form::open(array('url' => 'route')) }}

                            <div class="form-group">
                                {{ Form::label('code', 'Code') }}
                                {{ Form::text('code', null, ['class' => 'form-control', 'required' => true, 'maxlength'=>4, 'placeholder'=>'4 leters max' ]) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', null, ['class' => 'form-control', 'required']) }}
                            </div>

                            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                            {{ Form::close() }}

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.info-box-content -->
                </div>

            </div>
            <?php */  ?>

            <div class="col-md-4">
                <div class="box box-widget">

                </div>
            </div>
        </div>

        <!-- meter reading -->
        <div class="row">

            @include('customers.readings')

        </div>
        <!-- /. meter reading -->

        <!-- meters -->
        <div class="row">

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Meters</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive no-padding">

                        <table class="table table-hover table-responsive table-striped">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Parent</th>
                                <th>Serial</th>
                                <th>Route</th>
                                <th>Zone</th>
                                <th>Rate</th>
                                <th>Units</th>
                                <th>Billing Period</th>
                                <th>Operations</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($customer->meters as $row)

                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{ $row->getParent() }}</td>
                                    <td>{{$row->serial}}</td>
                                    <td>{{ $row->route->name }}</td>
                                    <td>{{ $row->zone->name }}</td>
                                    <td>{{$row->rate->name}}</td>
                                    <td>{{$row->meterUnit->display_name}}</td>
                                    <td>{{$row->billingPeriod->name}}</td>
                                    <td>
                                        <a href="{!! route('meters.edit', ['id'=>$row->id]) !!}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>

                                        {!! Form::open(['method' => 'DELETE', 'class'=>'inline', 'route' => ['meters.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm margin-r-5']) !!}
                                        {!! Form::close() !!}

                                        {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.remove-customer', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to remove  from customer ?")' ]) !!}
                                        {!! Form::submit('Remove from Customer', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}

                                        @if($row->outoforder == '0')
                                        {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.outoforder', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to set out of order ?")' ]) !!}
                                        {!! Form::submit('Set out of order', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                        @endif

                                    </td>
                                </tr>

                            @endforeach

                            </tbody>


                        </table>

                    </div>
                    <!-- /.box-body -->

                </div>
            </div>

        </div>
        <!-- /. meters -->

        <!-- assign meters to customer -->
        <div class="row">

            <div class="col-md-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Assign Meter to Customer</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="availableMeters" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Serial</th>
                                <th>MIU</th>
                                <th>Select</th>
                            </tr>
                            </thead>
                        </table>

                        <p>
                            <input type="button" class="btn btn-primary" id="saveTable" value="Add selected meters to {{ $customer->full_name }}"/>
                        </p>

                        <p id="selected-meters">
                            <input type="text" id="meters-data" value="" data-role="tagsinput"/>
                        </p>

                        <p>
                            <input type="button" class="btn btn-primary" id="assignMeters" value="Save"/>
                        </p>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>




        </div>
        <!-- /. assign meters to customer -->

        <!-- Add meter -->
        <div class="row">

            <div class="col-md-12">
                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Add Water Meter</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {!! form_start($form) !!}

                        <div class="col-md-6">
                            {!! form_until($form, 'meter_digits') !!}
                        </div>

                        <div class="col-md-6">
                            {!! form_until($form, 'read_type') !!}
                        </div>


                        <div class="col-md-12">
                            {!! form_row($form->submit) !!}
                        </div>

                    </div>

                </div>

            </div>

        </div>
        <!-- /. Add meter -->


	</div>
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

  var elt = $('#meters-data');
  elt.tagsinput({
    itemValue: 'meter',
    itemText: 'text',
    itemCustomer: 'customer',
  });


  $(document).ready(function () {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    })

    // Datatable Set
    var table = $('#availableMeters').DataTable({
      "ajax": "{{ route('meters.unasigned') }}",
      "language": {
        "emptyTable": "There are no unassigned Meters"
      },
      select: {
        style: 'os',
        selector: 'td:not(:last-child)' // no row selection on last column
      },
      /*
      "columnDefs": [{
        "targets": -1,
        "data": null,
        "defaultContent": "<button class='btn btn-sm btn-info'>Save!</button>"
      }],
      */

      columns: [
        {data: "id"},
        {data: "serial"},
        {data: "meter_miu"},
        {
          data: null,
          render: function (data, type, row) {
            if (type === 'display') {
              return '<input type="checkbox" class="form-check-input">';
            }
            return data;
          },
          className: "dt-body-center text-center"
        },
      ],
    });

    // Add selected meters to tags
    $(document).on('click', '#saveTable', function (e) {

      // elt.tagsinput('removeAll'); // TODO check with Kevin

      // iterate on every selected row
      table.rows().eq(0).each(function (index) {
        var row = table.row(index);
        var data = row.data();
        var node = row.node();

        let checkbox = $(node).find("input[type='checkbox']");
        if (checkbox.is(':checked')) {
          elt.tagsinput('add', {"meter": data['id'], "text": data['serial'], "customer": {{$customer->id}} });
        }
      })
    })


    // Assign meters to customer
    $(document).on('click', '#assignMeters', function(){

      var postData = elt.tagsinput('items');
      // var postData = [{meter: 3, customer: 1}, {meter: 20, customer: 10}];
      // console.log(postData);

      let request = $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/meters/assign',
        data: {data: postData},
      })

      request.done(function (data) {
        if ($.isEmptyObject(data.error)) {
          toastr.success(data.success, '', {
            positionClass: 'toast-top-full-width',
            timeOut: 5000
          })
          // table.ajax.reload();
          location.reload();

        } else {
          toastr.error(data.error, '', {
            positionClass: 'toast-top-full-width',
            progressBar: true,
            timeOut: 10000
          })
        }
      });

      request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR)
        console.log(textStatus)
        toastr.error(jqXHR.responseJSON.error.message, '', {positionClass: 'toast-top-full-width'});
      })

    })



    $('#availableMeters tbody').on('click', 'button', function () {

      let data = table.row( $(this).parents('tr') ).data();
      let row = $(this).parents('tr');
      let checkbox = row.find("input[type='checkbox']");

      // save assigned if checked

      if(checkbox.is(':checked')){

        // alert("ID is:  " + data['id'] + " will be assigned to " + {{ $customer->id }} );

        // TODO assign ID to customer
        let postData = {meter: data['id'], customer: {{ $customer->id }} };
        let request = $.ajax({
          dataType: 'json',
          type: 'POST',
          url: '/meters/assign',
          data: postData,
        })

        request.done(function(data){
          if ($.isEmptyObject(data.error)) {
            toastr.success(data.success, '', {
              positionClass: 'toast-top-full-width',
              timeOut: 5000
            })
            // table.ajax.reload();
            // location.reload();

          } else {
            toastr.error(data.error, '', {
              positionClass: 'toast-top-full-width',
              progressBar: true,
              timeOut: 10000
            })
          }
        });

        request.fail(function (jqXHR, textStatus) {
          console.log(jqXHR)
          console.log(textStatus)
          toastr.error(jqXHR.statusText, '', {positionClass: 'toast-top-full-width'});
        })
      } else {
        toastr.error('Please check assign before saving', '', {positionClass: 'toast-top-full-width'});

      }
    });
  });


</script>

@endpush