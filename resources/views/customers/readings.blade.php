<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Meter Readings</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->

        <div class="box-body table-responsive no-padding">

            <table class="table table-hover table-responsive table-striped">

                <thead>
                <tr>
                    <th>Meter Serial #</th>
                    <th>Name</th>
                    <th>Previous</th>
                    <th>Reading</th>
                    <th>Date</th>
                    <th>Units</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>

                @foreach($customer->meters as $row)
                    <tr>
                        <td>{{ $row->serial }}</td>
                        <td>{{ $row->customer->full_name }}</td>
                        <td>
                            @if($row->readings->last())
                                {{ $row->readings->last()->reading }}
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforeach


                @foreach($row->readings as $reading)
                    <tr>
                        <td>{{ $row->serial }}</td>
                        <td>{{ $row->route->name }}</td>

                        <td>{{ $reading->reading }}</td>
                        <td>{{ $reading->reading }}</td>
                        <td><input type="text"/></td>

                        <td>
                            <button class="btn btn-info btn-sm pull-left margin-r-5" data-meter="{{$row->id}}">Calculate Estimate</button>
                            <a href="{!! route('meters.edit', ['id'=>$row->id]) !!}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['meters.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach



                </tbody>


            </table>

        </div>
        <!-- /.box-body -->

    </div>
</div>
