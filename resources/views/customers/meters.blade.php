<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Meters</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->

        <div class="box-body table-responsive no-padding">

            <table class="table table-hover table-responsive table-striped">

                <thead>
                <tr>
                    <th>Id</th>
                    <th>Parent</th>
                    <th>Serial</th>
                    <th>Route</th>
                    <th>Zone</th>
                    <th>Rate</th>
                    <th>Units</th>
                    <th>Billing Period</th>
                    <th>Operations</th>
                </tr>
                </thead>

                <tbody>

                @foreach($customer->meters as $row)

                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{ $row->getParent() }}</td>
                        <td>{{$row->serial}}</td>
                        <td>{{ $row->route->name }}</td>
                        <td>{{ $row->zone->name }}</td>
                        <td>{{$row->rate->name}}</td>
                        <td>{{$row->meterUnit->display_name}}</td>
                        <td>{{$row->billingPeriod->name}}</td>
                        <td>
                            <a href="{!! route('meters.edit', ['id'=>$row->id]) !!}" class="btn btn-info btn-sm pull-left margin-r-5"><i class="fa fa-pencil"></i> Edit</a>

                            {!! Form::open(['method' => 'DELETE', 'class'=>'inline', 'route' => ['meters.destroy', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to delete ?")' ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm margin-r-5']) !!}
                            {!! Form::close() !!}

                            {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.remove-customer', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to remove  from customer ?")' ]) !!}
                            {!! Form::submit('Remove from Customer', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}

                            {!! Form::open(['method' => 'POST', 'class'=>'inline', 'route' => ['meters.outoforder', $row->id], 'onsubmit' => 'return confirm("Are you sure you want to set out of order ?")' ]) !!}
                            {!! Form::submit('Set out of order', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>

                @endforeach

                </tbody>


            </table>

        </div>
        <!-- /.box-body -->

    </div>
</div>