@extends('adminlte::page')

@section('htmlheader_title')
    | Edit Client
@endsection

@section('contentheader_title')
    Edit Client
@endsection

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">

                <div class="box">

                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Client</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->


                    {!! form_start($form) !!}
                    <div class="box-body">


                        {!! form_until($form, 'minimum_balance') !!}

                        {!! form_row($form->addresses) !!}
                    </div>

                    <div class="box-footer">
                        {!! form_end($form) !!}
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection
