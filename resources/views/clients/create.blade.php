@extends('adminlte::page')

@section('htmlheader_title')
    | Add Client
@endsection

@section('contentheader_title')

@endsection


@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! form($form) !!}
            </div>
        </div>
    </div>

@endsection
