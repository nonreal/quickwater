@extends('adminlte::page')

@section('htmlheader_title')
	Manage Clients
@endsection

@section('contentheader_title')
    Manage Clients
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Clients</h3>

                        <div class="pull-right box-tools">
                            <a href="{{ route('client.create') }}" class="btn btn-sm btn bg-orange">Add Client</a>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body no-padding">

                        <table class="table table-hover table-responsive table-striped">

                            <thead class="thead-inverse">
                            <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Operations</th>
                            </tr>
                            </thead>

                            <tbody>


                            @foreach($clients as $client)

                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$client->company_name}}</td>
                                    <td>{{$client->user->email}}</td>
                                    <td>
                                        <a href="{!! route('client.edit', ['id'=>$client->id]) !!}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                    </td>
                                </tr>
                                
                            @endforeach
                            
                            </tbody>
                            

                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>

			</div>
		</div>
	</div>
@endsection
