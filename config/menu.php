<?php

use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;
use App\Customer;

/*
Menu::macro('sidebar', function () {

	$id = request()->segment(2);

	$children = Menu::new();
	$children->addClass('nav child_menu');


	// Clients
	$nav = Menu::new();
	$nav->addClass('nav child_menu');
	$nav->routeIfCan('list-clients', 'clients.index', ' <i class="fa fa-folder-open" aria-hidden="true"></i>List');
	$nav->routeIfCan('create-clients', 'clients.create', '<i class="fa fa-plus" aria-hidden="true"></i>Add');
	$nav->routeIfCan('update-clients', 'clients.edit', '<i class="fa fa-pencil" aria-hidden="true"></i>Edit', [auth()->id()]);

	$children->submenuIfCan('list-clients', '<a href="#"><i class="fa fa-user"></i>Clients<span class="fa fa-chevron-down"></span></a>', $nav);

	// Water Rates
	$nav = Menu::new();
	$nav->addClass('nav child_menu');
	$nav->routeIfCan('list-water_rates', 'water-rates.index', ' <i class="fa fa-folder-open" aria-hidden="true"></i>List');
	$nav->routeIfCan('list-water_rates', 'water-rates.view', '<i class="fa fa-eye" aria-hidden="true"></i>View', ['id' => $id]);
	$nav->routeIfCan('create-water_rates', 'water-rates.create', '<i class="fa fa-plus" aria-hidden="true"></i>Add');
	$nav->routeIfCan('update-water_rates', 'water-rates.edit', '<i class="fa fa-pencil" aria-hidden="true"></i>Edit', ['id' => $id]);
	$children->submenuIfCan('list-water_rates', '<a href="#"><i class="fa fa-anchor"></i>Water Rates<span class="fa fa-chevron-down"></span></a>',
		$nav);


	if (auth()->user()->client()) {

		// Customers
		$nav = Menu::new();
		$nav->addClass('nav child_menu');
		$nav->routeIfCan('list-customers', 'customers.index', ' <i class="fa fa-folder-open" aria-hidden="true"></i> List');
		$nav->routeIfCan('list-customers', 'customers.view', '<i class="fa fa-eye" aria-hidden="true"></i>View', ['id' => $id]);

		if (auth()->user()->hasRole('client'))
			$nav->routeIfCan('create-customers', 'customers.create', '<i class="fa fa-plus" aria-hidden="true"></i>Add', [auth()->user()->userable->id]);

		$nav->routeIfCan('update-customers', 'customers.edit', '<i class="fa fa-pencil" aria-hidden="true"></i>Edit', ['id' => $id]);
		$children->submenuIfCan('list-customers', '<a href="#"><i class="fa fa-user"></i>Customers<span class="fa fa-chevron-down"></span></a>',
			$nav);

	}

	$menu = Menu::new();
	$menu->addClass('nav side-menu');
	$menu->submenu('<a><i class="fa fa-home"></i>Super Admin Menu<span class="fa fa-chevron-down"></span></a>', $children);

	return $menu;

});
*/

/*
Menu::macro('adminlteSubmenu', function ($submenuName, $icon = null) {
	return Menu::new()->prepend('<a href="#"><i class="fa fa-'.$icon.'"></i><span> ' . $submenuName . '</span> <i class="fa fa-angle-left pull-right"></i></a>')
		->addParentClass('treeview')->addClass('treeview-menu');
});


Menu::macro('adminlteMenu', function () {
	return Menu::new()
		->addClass('sidebar-menu tree')
		->setAttribute('data-widget', 'tree');
		;
});


Menu::macro('adminlteSeparator', function ($title) {
	return Html::raw($title)->addParentClass('header');
});


Menu::macro('adminlteDefaultMenu', function ($content) {
	return Html::raw('<i class="fa fa-link"></i><span>' . $content . '</span>')->html();
});


Menu::macro('sidebar', function () {

	// dd($customer->toArray());

	return Menu::adminlteMenu()
		->add(Html::raw('HEADER')->addParentClass('header'))

		->action('HomeController@index', '<i class="fa fa-home"></i><span>Home</span>')
		// ->add(Menu::adminlteSeparator('Acacha Adminlte'))
		#adminlte_menu
		->routeIfCan('list-clients', 'clients.index', '<i class="fa fa-user" aria-hidden="true"></i><span>Clients</span>')
		->routeIfCan('list-customers', 'customers.index', '<i class="fa fa-user" aria-hidden="true"></i><span>Customers</span>')
		->routeIfCan('list-meters', 'meters.index', '<i class="fa fa-clock-o" aria-hidden="true"></i><span>Meters</span>')
		->routeIfCan('list-rates','water-rates.index', '<i class="fa fa-bar-chart" aria-hidden="true"></i><span>Water Rates</span>')

		->add(
			Menu::adminlteSubmenu('Customers', 'user')
				->routeIfCan('list-customers', 'customers.index', '<i class="fa fa-user" aria-hidden="true"></i><span>List</span>')
		)

		->add(
			Menu::adminlteSubmenu('Configuration', 'gear')
				->route('meter-type.index', 'Meter Types')
				->route('route.index', 'Routes')
				->route('zone.index', 'Zones')
				->route('tax-code.index', 'Tax Codes')
				->route('billing-period.index', 'Billing Periods')
				->route('custom-charge.index', 'Custom Charges')
		)
		->add(
			Menu::adminlteSubmenu('Roles & Permissions', 'key')
				// ->prepend('<a href="#"><i class="fa fa-key"></i><span>Roles & Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>')
				->routeIfCan('edit-roles-permissions', 'users.index', ' <i class="fa fa-user" aria-hidden="true"></i>Users')
				->routeIfCan('edit-roles-permissions', 'roles.index', ' <i class="fa fa-key" aria-hidden="true"></i>Roles')
				->routeIfCan('edit-roles-permissions', 'permissions.index', ' <i class="fa fa-key" aria-hidden="true"></i>Permissions')
		)

		->setActiveFromRequest();
});

*/