<?php

namespace App;

class Zone extends Model
{

    protected $table = 'zones';
    public $timestamps = false;

    public function meters()
    {
        return $this->hasMany(Meter::class);
    }

}