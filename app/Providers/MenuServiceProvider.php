<?php

namespace App\Providers;

use App\Customer;
use Illuminate\Support\ServiceProvider;
use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Menu;
use Spatie\Menu\Laravel\Link;

class MenuServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{


	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//

		Menu::macro('adminlteSubmenu', function ($submenuName, $icon = null) {
			return Menu::new()->prepend('<a href="#"><i class="fa fa-' . $icon . '"></i><span> ' . $submenuName . '</span> <i class="fa fa-angle-left pull-right"></i></a>')
				->addParentClass('treeview')->addClass('treeview-menu');
		});


		Menu::macro('adminlteMenu', function () {
			return Menu::new()
				->addClass('sidebar-menu tree')
				->setAttribute('data-widget', 'tree');;
		});


		Menu::macro('adminlteSeparator', function ($title) {
			return Html::raw($title)->addParentClass('header');
		});


		Menu::macro('adminlteDefaultMenu', function ($content) {
			return Html::raw('<i class="fa fa-link"></i><span>' . $content . '</span>')->html();
		});

		Link::macro('customer', function (Customer $customer) {
			return Link::toAction('CustomerController@show', $customer->full_name, [$customer->id]);
		});


		Menu::macro('sidebar', function () {

			return Menu::adminlteMenu()
				->add(Html::raw('HEADER')->addParentClass('header'))
				->action('HomeController@index', '<i class="fa fa-home"></i><span>Home</span>')
				// ->add(Menu::adminlteSeparator('Acacha Adminlte'))
				#adminlte_menu
				->routeIfCan('list-clients', 'clients.index', '<i class="fa fa-user" aria-hidden="true"></i><span>Clients</span>')
				->routeIfCan('list-customers', 'customers.index', '<i class="fa fa-user" aria-hidden="true"></i><span>Customers</span>')
				->routeIfCan('list-meters', 'meters.index', '<i class="fa fa-clock-o" aria-hidden="true"></i><span>Meters</span>')
				->routeIfCan('list-rates', 'water-rates.index', '<i class="fa fa-bar-chart" aria-hidden="true"></i><span>Water Rates</span>')
				/*
				->add(
					Menu::adminlteSubmenu('Customers', 'user')
						->routeIfCan('list-customers', 'customers.index', '<i class="fa fa-user" aria-hidden="true"></i><span>List</span>')
				)
				*/
				->add(
					Menu::adminlteSubmenu('Configuration', 'gear')
						->route('meter-type.index', 'Meter Types')
						->route('route.index', 'Routes')
						->route('zone.index', 'Zones')
						->route('tax-code.index', 'Tax Codes')
						->route('billing-period.index', 'Billing Periods')
						->route('custom-charge.index', 'Custom Charges')
				)
				->add(
					Menu::adminlteSubmenu('Roles & Permissions', 'key')
						// ->prepend('<a href="#"><i class="fa fa-key"></i><span>Roles & Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>')
						->routeIfCan('edit-roles-permissions', 'users.index', ' <i class="fa fa-user" aria-hidden="true"></i>Users')
						->routeIfCan('edit-roles-permissions', 'roles.index', ' <i class="fa fa-key" aria-hidden="true"></i>Roles')
						->routeIfCan('edit-roles-permissions', 'permissions.index', ' <i class="fa fa-key" aria-hidden="true"></i>Permissions')
				)
				->setActiveFromRequest();
		});





	}
}
