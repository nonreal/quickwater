<?php

namespace App;

class Reading extends Model
{

    protected $table = 'readings';
    public $timestamps = true;

    public function meter()
    {
        return $this->belongsTo(Meter::class);
    }


	public function scopeLastNth($query, $count)
	{

		$readings = $query->take($count)->latest();

		return $readings;

    }

}