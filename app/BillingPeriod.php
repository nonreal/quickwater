<?php

namespace App;

class BillingPeriod extends Model
{
    //

	public function periodUnit()
	{
		return $this->belongsTo(BillingPeriodUnit::class, 'billing_period_unit_id');
	}

}
