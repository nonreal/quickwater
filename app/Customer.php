<?php

namespace App;

use Arcanedev\LaravelNotes\Models\Note;
use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lecturize\Addresses\Traits\HasAddresses;
use Request;


class Customer extends Model
{
    //
	use SoftDeletes;
	use HasAddresses;
	use HasManyNotes;

	protected $guarded = ['addresses'];


	public function user()
	{
		return $this->morphOne(User::class, 'userable');
	}

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function meters()
	{
		return $this->hasMany(Meter::class);
	}


	public function getFullNameAttribute()
	{
		return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
	}

	/**
	 * Associate address array to Customer
	 * @param $data
	 */
	public function addUpdateAddresses($data)
	{

		foreach ($data as $address) {
			if (!isset($address['delete_address']))
				$this->addAddress($address);
		}
	}

	/**
	 * @param $data array from form in \App\Http\Controllers\MeterController::store
	 * @return void redirect back
	 */
	public function addWaterMeter($data)
	{

		// dd($this->toArray());
		$water_meter_id = MeterType::where('code', 'W')->pluck('id')->first();

		// Create Meter
		$meter = new Meter();
		// $meter->meter_type_id = MeterType::where('code', 'W')->pluck('id')->first();
		$meter->fill($data);
		$meter->meterType()->associate($water_meter_id);

		$meter->customer()->associate($this);
		$meter->save();

		flash('Meter created.')->success();

		return redirect()->route('customers.show', ['id' => $this->id]);

	}


}
