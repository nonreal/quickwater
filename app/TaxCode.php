<?php

namespace App;

class TaxCode extends Model
{

    protected $table = 'tax_codes';
    public $timestamps = false;


	public function rates() {
		return $this->belongsToMany(Rate::class);
	}

}