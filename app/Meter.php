<?php

namespace App;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Lecturize\Addresses\Traits\HasAddresses;

class Meter extends Model 
{

    protected $table = 'meters';
    public $timestamps = true;

	use HasAddresses;
	use HasManyNotes;

	protected $guarded = ['addresses'];

	// protected $with = ['meterType', 'meterUnit', 'route', 'zone', 'rate',  'readings', 'billingPeriod'];


	public function parent()
	{
		return $this->belongsTo(Meter::class, 'parent_id');
	}

	public function children()
	{
		return $this->hasMany(Meter::class, 'parent_id');
	}


	public function meterType()
    {
    	return $this->belongsTo(MeterType::class);
    }

	public function meterUnit()
	{
		return $this->belongsTo(MeterUnit::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

	public function rate()
	{
		return $this->belongsTo(Rate::class);
	}

    public function readings()
    {
        return $this->hasMany(Reading::class)->orderByDesc('id');
    }

	public function readingsLast($amount)
	{
		return $this->hasMany(Reading::class)->lastNth($amount);
    }

	public function customer()
	{
		return $this->belongsTo(Customer::class);
    }

	public function billingPeriod()
	{
		return $this->belongsTo(BillingPeriod::class);
    }


	public function scopeWater()
	{

		$meter_type_id = MeterType::where('code', 'W')->pluck('id')->first();

		return $this->where('meter_type_id', $meter_type_id);

    }

	public function scopeUnasigned()
	{
		return $this->whereNull('customer_id');
	}


	public function getParent()
	{
		if($this->parent)
			return $this->parent->id;
	}


	public function outOfOrder()
	{
		$this->update(['outoforder'=> '1']);
	}

}