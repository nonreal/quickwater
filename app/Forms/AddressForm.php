<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Webpatser\Countries\Countries;
use Lecturize\Addresses\Models\Address;

class AddressForm extends Form
{
    public function buildForm()
    {
        // Add fields here...

	    $this
		    ->add('line1', 'text', [
			    'rules' => 'required',
			    'label' => 'Line 1',
		    ])

		    ->add('line2', 'text', [
			    'rules' => 'required',
			    'label' => 'Line 2',
		    ])

		    ->add('street', 'text', [
	    	    'rules' => 'required',
			    'label' => 'Address line 3 (Street line)',
	        ])

		    ->add('city', 'text', [
			    'rules' => 'required',
			    'label' => 'Address line 4 (City / Nearest city)',
		    ])

		    ->add('state', 'text', [
			    'rules' => 'required',
			    'label' => 'Address line 5 (State / County)',
		    ])

		    ->add('post_code', 'text', [
			    'rules' => 'required',
			    'label' => 'Post code / Eircode / Zip code',
		    ])

		    ->add('country_id', 'entity', [
		    	'label' => 'Country',
		    	'class' => 'Webpatser\Countries\Countries',
			    'query_builder' => function (Countries $countries) {
				    return $countries->pluck('name', 'id')->toArray();
			    },

			    'multiple' => false,
			    'expanded' => false,
		    ])

			/*
		    ->add('is_billing', 'checkbox', [
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'value' => 1,
		    ])

		    ->add('is_service', 'checkbox', [
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'value' => 1,
		    ])

		    ->add('delete_address', 'checkbox', [
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'value' => 1,
		    ])
			*/
			

	    ;
    }
}
