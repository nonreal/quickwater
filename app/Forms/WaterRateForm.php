<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\WaterRate;

class WaterRateForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
	    $this
		    ->add('code', 'text', [
	    	'rules' => 'required|max:3',
	        ])
		    ->add('name', 'text',[
		    	'rules' => 'required',
		    ])
		    ->add('base_charge', 'number', [
			    'rules' => 'required|numeric|between:0,9999.99'
		    ])

		    ->add('taxable', 'checkbox', [
		    	'value' => 1,
			    // 'wrapper' => ['class' => 'checkbox'],
			    'label_show' => true,
		    ])
		    ->add('misc_amount', 'number', [
			    'rules' => 'required|numeric'
		    ])
		    ->add('misc_amount_desc', 'text', [
			    'rules' => '',
		    ])

		    ->add('rounding', 'choice', [
			    'choices' => WaterRate::getPossibleEnumValues('rounding'),
			    'attr' => [
				    'class' => 'form-control col-md-6',
			    ],
		    ])

		    /*
			->add('rounding', 'entity', [
			    'class' => WaterRate::class,
			    'query_builder' => function (WaterRate $waterRate) {
				    return $waterRate->getPossibleEnumValues('rounding');
			    },

			    'multiple' => false,
			    'expanded' => false,
		    ])
		    */

		    ->add('round_highest_bp_value', 'checkbox', [
			    'value' => 1,
			    // 'wrapper' => ['class' => 'checkbox'],
			    'label_show' => true,
		    ])
		    ->add('usage_charge_highest_value', 'checkbox', [
			    'value' => 1,
			    // 'wrapper' => ['class' => 'checkbox'],
			    'label_show' => true,
		    ])
		    ->add('breakpoints', 'choice', [
			    'choices' => WaterRate::getPossibleEnumValues('breakpoints'),
			    'choice_options' => [
				    'wrapper' => ['class' => 'choice-wrapper'],
				    'label_attr' => ['class' => 'label-class'],
			    ],
			    'expanded' => false,
			    'multiple' => false,
		    ])

		    ->add('waterUsageRates', 'collection', [
			    'type' => 'form',
			    'property' => 'id',
			    'prototype' => true,
			    'wrapper' => ['class' => 'usage-rates'],
			    'options' => [    // these are options for a single type
				    'class' => 'App\Forms\WaterUsageRateForm',
				    'label' => false,
				    'wrapper' => array('class' => 'usage-rate'),
			    ],
			    'label_show' => false,
		    ])
	    ;

	    $this->add('submit', 'submit', [
		    'wrapper' => ['class' => 'clearfix'],
		    'attr' => [
			    'class' => 'btn btn-primary btn-block btn-lg'
		    ]
	    ]);

    }
}
