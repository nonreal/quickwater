<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ClientForm extends Form
{
    public function buildForm()
    {
        // Add fields here...

	    $this

		    ->add('company_name', 'text', [
			    'rules' => 'required|min:5',
		    ])

	        ->add('first_name', 'text', [
		        'rules' => 'required',
	        ])
		    ->add('last_name', 'text', [
		    	'rules' => 'required'
		    ])
		    ->add('service_charge_rate', 'number', [
		    	'rules' => 'required'
		    ])
		    ->add('late_fee_amount', 'number',[
			    'rules' => 'required'
		    ])
		    ->add('minimum_balance', 'number',[
//			    'rules' => 'required'
		    ])

		    ->add('addresses', 'collection', [
			    'type' => 'form',
			    'property' => 'id',
			    'prototype' => true,
			    'wrapper' => ['class' => 'form-group grid-3_xs-1'],
			    'options' => [    // these are options for a single type
				    'class' => AddressForm::class,
				    'label' => false,
				    'wrapper' => array('class'=> 'col'),
			    ],
			    'label_show' => false,
		    ])
	    ;

	    $this->add('Save', 'submit', [
	    	'attr' => [
			    'class' => 'btn btn-primary'
		    ]
	    ]);

    }
}
