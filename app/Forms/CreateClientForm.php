<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CreateClientForm extends Form
{
    public function buildForm()
    {
        // Add fields here...

	    $this->add('client', 'form', [
		    'class' => ClientForm::class,
		    'wrapper' => [ 'class' => 'col-md-6 col-sm-12 col-xs-12'],
//		    'label_show' => true,
	    ]);

	    $this->add('user', 'form', [
		    'class' => UserForm::class,
		    'wrapper' => [ 'class' => 'col-md-6 col-sm-12 col-xs-12'],
//		    'label_show' => true,
	    ]);

	    $this->user->removeChild('name');
	    $this->user->removeChild('submit');


    }
}
