<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Lecturize\Addresses\Facades\Addresses;

class CustomerForm extends Form
{
    public function buildForm()
    {
	    $this
	        ->add('first_name', 'text', [
		        'rules' => 'required',
	        ])
		    ->add('last_name', 'text', [
		    	'rules' => 'required'
		    ])
		    ->add('phone', 'text', [
			    'rules' => 'required',
			    'attr' => [
				    'placeholder' => 'Land Line',
			    ],
		    ])
		    ->add('phone_extra', 'text', [
			    // 'rules' => 'required',
			    'attr' => [
				    'placeholder' => 'Land Line',
			    ],
		    ])
		    ->add('mobile_phone', 'text', [
			    // 'rules' => 'required',
			    'attr' => [
			    	'placeholder' => 'Mobile Phone',
			    ],
		    ])
		    ->add('mobile_phone_extra', 'text', [
			    // 'rules' => 'required',
			    'attr' => [
				    'placeholder' => 'Mobile Phone',
			    ],
		    ])
		    ->add('email_extra', 'email', [
			    'rules' => 'email|max:255|unique:customers,email_extra,' . $this->getData('id'),
		    ])

		    ->add('addresses', 'collection', [
			    'type' => 'form',
			    'property' => 'id',
			    'prototype' => true,
			    'wrapper' => ['class' => 'form-group grid-3_xs-1'],
			    'options' => [    // these are options for a single type
				    'class' => 'App\Forms\AddressForm',
				    'label' => false,
				    'wrapper' => array('class'=> 'col'),
			    ],
			    'label_show' => false,
		    ])
	    ;

	    $this->add('submit', 'submit', [
	    	'attr' => [
			    'class' => 'btn btn-primary'
		    ]
	    ]);

    }
}
