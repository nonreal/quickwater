<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class EditClientForm extends Form
{
    public function buildForm()
    {
        // Add fields here...

	    $this->add('client', 'form', [
		    'class' => ClientForm::class,
		    'wrapper' => [ 'class' => 'col-md-6 col-sm-12 col-xs-12'],
		    'label' => false,
	    ]);


	    $this->add('addresses', 'form', [
		    'class' => AddressForm::class,
		    'wrapper' => [ 'class' => 'col-md-6 col-sm-12 col-xs-12'],
		    'label' => false,
	    ]);


    }
}
