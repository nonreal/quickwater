<?php

namespace App\Forms;

use App\BillingPeriod;
use App\Meter;
use App\MeterType;
use App\MeterUnit;
use App\Rate;
use App\Zone;
use App\Route;
use Kris\LaravelFormBuilder\Form;

class WaterMeterForm extends Form
{

	public function buildForm()
    {

	    $this

		    //Set as Child
		    ->add('parent_id', 'entity', [
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Parent',
			    'attr' => ['class' => 'form-control select2'],
			    'class' => Meter::class,
			    'query_builder' => function (Meter $meter) {
		    	    return $meter->pluck('serial', 'id')->toArray();
			    },
		    ])

		    // Route
		    ->add('route_id', 'entity', [
			    'rules' => 'required',
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Route',
			    'class' => Route::class,
			    'query_builder' => function (Route $route) {
				    return $route->pluck('name', 'id')->toArray();
			    },
		    ])

		    // Zone
		    ->add('zone_id', 'entity', [
			    'rules' => 'required',
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Zone No',
			    'class' => Zone::class,
			    'query_builder' => function (Zone $zone) {
				    return $zone->pluck('name', 'id')->toArray();
			    },
		    ])
		    
		    // Rate
		    ->add('rate_id', 'entity', [
			    'rules' => 'required',
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Rate',
			    'class' => Rate::class,
			    'query_builder' => function (Rate $rate) {
				    return $rate->pluck('name', 'id')->toArray();
			    },
		    ])

		    // Meter Unit
		    ->add('meter_unit_id', 'entity', [
			    'rules' => 'required',
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Units',
			    'class' => MeterUnit::class,
			    'query_builder' => function (MeterUnit $meterUnit) {
				    return $meterUnit->where('meter_type', MeterType::where('code', 'W')->pluck('id')->first())->pluck('display_name', 'id')->toArray();
			    },
		    ])
		    // Billing Period
		    ->add('billing_period_id', 'entity', [
			    'rules' => 'required',
			    'empty_value' => trans('default.empty_option'),
			    'label' => 'Billing Period',
			    'class' => BillingPeriod::class,
			    'query_builder' => function (BillingPeriod $billingPeriod) {
				    return $billingPeriod->pluck('name', 'id')->toArray();
			    },
		    ])

		    /*
		    ->add('billing_start', 'date', [
			    'rules' => 'required',
		    ])
		    ->add('billing_end', 'date', [
			    // 'rules' => 'required',
		    ])
		    ->add('installed', 'date', [
			    'rules' => 'required',
		    ])

		    ->add('lot_no', 'text', [
			    'rules' => 'required|max:4',
		    ])
		    */

		    ->add('meter_digits', 'number', [
			    'label' => 'Meter Digits No',
			    'rules' => 'required|numeric',
		    ])

		    ->add('serial', 'number', [
		    	'label' => 'Meter Serial No',
			    'rules' => 'required|numeric',
		    ])
		    ->add('meter_miu', 'text', [
			    'label' => 'Meter MIU No',
			    'rules' => 'required',
		    ])
		    ->add('meter_id', 'text', [
			    'label' => 'Meter ID',
			    'rules' => 'required|regex:/^\d{4,}-?\d*$/m',

		    ])

		    ->add('location', 'text', [
		    	'rules' => 'required',
		    ])
		    ->add('multiplier', 'number', [
			    'rules' => 'required|numeric|between:0,100',
		    ])
		    ->add('read_type', 'text', [
		    	'rules' => 'required|max:1',
		    ])
		    ->add('outoforder', 'choice', [
			    'choices' => [1 => 'Yes', 0 => 'No'],
			    'choice_options' => [
				    'wrapper' => ['class' => 'choice-wrapper'],
				    'label_attr' => ['class' => 'label-class'],
			    ],
			    'expanded' => true,
			    'multiple' => false,
			    'help_block' => [
				    'text' => 'Set Meter as Out of Order',
				    'tag' => 'p',
			    ],
		    ])


		    ->add('addresses', 'collection', [
			    'type' => 'form',
			    'property' => 'id',
			    'prototype' => true,
			    'wrapper' => ['class' => 'form-group grid-3_xs-1'],
			    'options' => [    // these are options for a single type
				    'class' => 'App\Forms\AddressForm',
				    'label' => false,
				    'wrapper' => array('class' => 'col'),
			    ],
			    'label_show' => false,
		    ])


		    /*
		    ->add('source', 'choice', [
			    'rules' => 'required',
		    	'label' => 'Water Source No',
			    'choices' => Meter::getPossibleEnumValues('source'),
		    ])
		    */

		;

	    $this->add('submit', 'submit', [
		    'attr' => [
			    'class' => 'btn btn-primary'
		    ]
	    ]);

    }
}
