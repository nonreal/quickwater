<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
            	'rules' => 'required',
            ])
            ->add('email', 'email', [
            	'rules' => 'required|email|max:255|unique:users,email',
            ])
	        ->add('password', 'repeated', [
	        	'rules' => 'required|min:6',
		        'type' => 'password',
                'second_name' => 'password_confirmation',
			])
        ;

	    $this->add('submit', 'submit', [
		    'attr' => [
			    'class' => 'btn btn-primary'
		    ]
	    ]);
    }
}
