<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class UsageRateForm extends Form
{
    public function buildForm()
    {
        // Add fields here...

	    $this
		    ->add('id', 'hidden')

		    ->add('usage_rate', 'number', [
		        'rules' => 'required|numeric',
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'attr' => [
				    'placeholder' => '0.000000',
			    ],
	        ])
		    ->add('break_point', 'number', [
			    'rules' => 'required|numeric',
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'attr' => [
				    'placeholder' => '0',
			    ],
		    ])
		    ->add('progressive_base_charge', 'number', [
			    'rules' => 'required|numeric',
			    'wrapper' => array('class' => 'form-group col-xs-6 col-md-4'),
			    'attr' => [
				    'placeholder' => '0.00',
			    ],
		    ])

	    ;
    }
}
