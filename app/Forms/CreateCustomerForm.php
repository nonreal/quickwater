<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CreateCustomerForm extends Form
{
    public function buildForm()
    {

	    $this->add('customer', 'form', [
		    'class' => CustomerForm::class,
		    // 'wrapper' => ['class' => 'col-md-6 col-sm-12 col-xs-12'],
		    'label' => '__NEW CUSTOMER LABEL__',
		    'label_show' => false,
	    ]);

	    $this->add('user', 'form', [
		    'class' => UserForm::class,
		    // 'wrapper' => ['class' => 'col-md-6 col-sm-12 col-xs-12'],
		    'label' => '__NEW CLIENT LABEL__',
		    'label_show' => false,
	    ]);

	    $this->user->removeChild('name');
	    $this->user->removeChild('submit');


    }
}
