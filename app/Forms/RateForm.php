<?php

namespace App\Forms;

use App\CustomCharge;
use App\TaxCode;
use Kris\LaravelFormBuilder\Form;

class RateForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
	    $this
		    ->add('code', 'text', [
	    	    'rules' => 'required|max:4',
	        ])
		    ->add('name', 'text',[
		    	'rules' => 'required',
		    ])
		    ->add('customCharges', 'entity', [

			    'choice_options' => [
				    'attr' => ['class' => 'form-control select2'],
			    ],
			    'label' => 'Custom Charges',
			    'class' => CustomCharge::class,
			    'choices' => CustomCharge::all()->pluck('name', 'id')->toArray(),
			    // 'wrapper' => array('class' => 'form-control select2'),
			    'multiple' => true,
			    'expanded' => false,
			    'help_block' => [
				    'text' => 'Use CTRL to select multiple choices',
				    'tag' => 'p',
			    ],
		    ])

		    //taxCodes
		    ->add('usageTaxCodes', 'entity', [
			    'wrapper' => ['class' => 'form-group col-md-6'],
			    'label' => 'Usage Tax',
			    'class' => TaxCode::class,
			    'choices' => TaxCode::all()->pluck('name', 'id')->toArray(),
			    'empty_value' => '=== Select Tax ===',
		    ])

		    ->add('pbcTaxCodes', 'entity', [
			    'wrapper' => ['class' => 'form-group col-md-6'],
			    'label' => 'Progressive Base Charge Tax',
			    'class' => TaxCode::class,
			    'choices' => TaxCode::all()->pluck('name', 'id')->toArray(),
			    'empty_value' => '=== Select Tax ===',
		    ])

		    /*
		    // Usage Tax Code
		    ->add('usage_tax_code', 'entity', [
			    'wrapper' => ['class' => 'form-group col-md-6'],
			    'label' => 'Usage Tax',
			    'class' => TaxCode::class,
			    'choices' => TaxCode::all()->pluck('name', 'id')->toArray(),
			    'empty_value' => '=== Select Tax ===',
		    ])

		    // Progressive Base Charge Tax Code
		    ->add('pbc_tax_code', 'entity', [
			    'wrapper' => ['class' => 'form-group col-md-6'],
			    'label' => 'Progressive Base Charge Tax',
			    'empty_value' => '=== Select Tax ===',
			    'class' => TaxCode::class,
			    'choices' => TaxCode::all()->pluck('name', 'id')->toArray(),
		    ])
		    */

		    ->add('usageRates', 'collection', [
			    'type' => 'form',
			    'property' => 'id',
			    'prototype' => true,
			    'wrapper' => ['class' => 'usage-rates'],
			    'options' => [    // these are options for a single type
				    'class' => UsageRateForm::class,
				    'label' => false,
				    'wrapper' => array('class' => 'usage-rate'),
			    ],
			    'label_show' => false,
		    ])
	    ;

	    $this->add('submit', 'submit', [
		    'attr' => [
			    'class' => 'btn btn-primary'
		    ]
	    ]);

    }
}
