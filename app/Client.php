<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Lecturize\Addresses\Traits\HasAddresses;

class Client extends Model
{
	//
	use HasAddresses;
	use SoftDeletes;


	protected $fillable = [
		'company_name', 'first_name', 'last_name', 'service_charge_rate', 'late_fee_amount', 'minimum_balance'
	];

	//	$client = Client::with('user', 'addresses.country')->get();
	//	protected $guarded = ['addresses'];
	//	protected $with = ['user', 'addresses', 'addresses.country'];


	public function user() {

		return $this->morphOne(User::class, 'userable');
	}

	public function rates()
	{
		return $this->hasMany(Rate::class);
	}

	public function customers() {

		return $this->hasMany(Customer::class);
	}

	public function roles() {
		return $this->user->roles();
	}

	public function getFullNameAttribute() {

		return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
	}


}
