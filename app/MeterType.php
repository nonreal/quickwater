<?php

namespace App;

class MeterType extends Model
{

    protected $table = 'meter_types';
    public $timestamps = false;

	public function meters()
	{
		return $this->hasMany(Meter::class);
	}

    public function rates()
    {
        return $this->belongsToMany(Rate::class);
    }

}