<?php

namespace App;

class CustomCharge extends Model
{

	public $timestamps = false;

	public function rates()
	{
		return $this->belongsToMany(Rate::class)->withTimestamps();
	}

}
