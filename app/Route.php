<?php

namespace App;

class Route extends Model
{

    protected $table = 'routes';
    public $timestamps = false;

    public function meters()
    {
        return $this->hasMany(Meter::class);
    }

}