<?php

namespace App;

class UsageRate extends Model
{

    protected $table = 'usage_rates';
    public $timestamps = true;

    protected $fillable = ['usage_rate', 'break_point', 'progressive_base_charge'];

    public function rate()
    {
        return $this->belongsTo(Rate::class);
    }

}