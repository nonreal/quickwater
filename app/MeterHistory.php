<?php

namespace App;

class MeterHistory extends Model
{
	public $timestamps = true;

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}


	public function meter()
	{
		return $this->belongsTo(Meter::class);
	}
}
