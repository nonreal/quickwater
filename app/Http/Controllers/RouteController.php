<?php

namespace App\Http\Controllers;

use App\Route;

class RouteController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		$rows = Route::all();

		return view('select.route.index', compact('rows'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('select.route.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);

		Route::create(request()->only('code', 'name'));

		flash('Route Added')->success();

		return redirect()->route('route.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id) {

		$entry = Route::findOrFail($id); //Get user with specified id

		return view('select.route.edit', compact('entry')); //pass user and roles data to view
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id) {

		$row = Route::findOrFail($id); //Get role specified by id

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);
		$input = request()->only(['name', 'code']);
		$row->fill($input)->save();

		flash('Route Updated')->success();

		return redirect()->route('route.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id) {
		$row = Route::findOrFail($id);
		$row->delete();

		flash('Route Deleted')->success();

		return redirect()->route('route.index');
	}

}

?>