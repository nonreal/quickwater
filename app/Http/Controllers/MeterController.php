<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Forms\WaterMeterForm;
use App\Meter;

use App\MeterFilters;
use App\MeterHistory;
use Auth;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Validator;


class MeterController extends Controller
{

	use FormBuilderTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{


		$meters = Meter::all();


		$meters = Meter::latest()->paginate(20);

		if($request->exists('outoforder')){
			$meters = Meter::where('outoforder', 1)->latest()->paginate(20)->appends('outoforder', 1);
		}

		if($request->exists('no_customer')){
			$meters = Meter::where('customer_id', null)->latest()->paginate(20)->appends('no_customer', 1);
		}

		return view('meters.index', compact('meters'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Customer $customer
	 * @param Request $request
	 * @return Response
	 */
	public function store(Customer $customer, Request $request)
	{

		// add a meter to customer
		$form = $this->form(WaterMeterForm::class);
		$form->redirectIfNotValid();
		$data = $form->getFieldValues(false);

		$customer->addWaterMeter($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show(Meter $meter)
	{
		return view('customers.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Meter $meter
	 * @return Response
	 * @internal param int $id
	 */
	public function edit(Meter $meter)
	{

		$form = $this->form(WaterMeterForm::class, [
			'method' => 'PUT',
			'model' => $meter,
			'url' => route('meters.update', ['id' => $meter->id]),
		]);

		$rows = $meter->notes;

		return view('meters.edit', compact('form', 'meter', 'rows'));


	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Meter $meter
	 * @return Route
	 * @internal param int $id
	 */
	public function update(Meter $meter)
	{

		$data = request()->all();

		$form = $this->form(WaterMeterForm::class);
		$form->redirectIfNotValid();

		$meter->update($data);


		//remove all addresses
		$meter->flushAddresses();

		foreach ($data['addresses'] as $address) {
			if (!isset($address['delete_address']))
				$meter->addAddress($address);
		}

		flash('Meter edited.')->success();

		return redirect()->route('meters.index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

	/**
	 * @param Meter $meter
	 * @return Route
	 */
	public function removeCustomer(Meter $meter)
	{

		if(!$meter->customer){
			flash('No existing customer')->error();
			return redirect()->back();
		}

		$log = new MeterHistoryController();
		$log->log($meter->id, $meter->customer->id, $log->actionRemove);


		$meter->customer()->dissociate();
		$meter->save();
		flash('Meter edited.')->success();

		return redirect()->back();
		// return redirect()->route('meters.index');

	}


	public function addCustomer(Request $request)
	{


		$response = [];

		foreach($request->data as $data){

			$meter = Meter::find($data['meter']);
			if ($meter == null){
				// return response()->json(['error' => 'Invalid Meter!']);
				$response['error'][] = 'Meter ' . $data['meter'] . ' is invalid!';

			}

			$customer = Customer::find($data['customer']);
			if ($customer == null){
				// return response()->json(['error' => 'Invalid Customer!']);
				$response['error'][] = 'Customer ' . $data['customer'] . ' is invalid!';
			}

			$meter->customer()->associate($customer);
			$meter->save();

			$response['success'][] = 'Meter ' . $data['meter'] . ' added to customer!';
		}

		return response()->json($response);

	}


	public function setOutofOrder(Meter $meter)
	{

		$meter->outOfOrder();

		flash('Meter edited.')->success();
		return redirect()->route('meters.index');
	}

	public function addNote(Request $request, Meter $meter)
	{
		$rules = array(
			'content' => 'required',
		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			flash('Note is required')->error();

			return redirect()->back();
		}

		$meter->createNote($request->input('content'), Auth::user());

		flash('Note added.')->success();

		return redirect()->back();

	}


	public function getUnasigned()
	{

		$meters = Meter::water()->unasigned()->get(['id', 'serial', 'meter_miu']);
		$response = array('data' => $meters);

		return response()->json($response);

	}

}

?>