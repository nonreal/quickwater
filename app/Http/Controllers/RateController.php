<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Forms\RateForm;
use App\UsageRate;
use DB;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;


class RateController extends Controller
{

	use FormBuilderTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rows = Rate::all();

		return view('rates.index', compact('rows'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param FormBuilder $formBuilder
	 * @return Response
	 */
	public function create(FormBuilder $formBuilder)
	{
		$form = $formBuilder->create(RateForm::class, [
			'method' => 'POST',
			'url' => route('rates.store'),
			// 'language_name' => 'translations',
		]);

		return view('rates.create', compact('form'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $meter_type_id
	 * @return Response
	 * @internal param Request $request
	 * @internal param $data
	 */
	public function store($meter_type_id)
	{

		$form = $this->form(RateForm::class);
		$form->redirectIfNotValid();

		// Set meter type to W by default
		request()->request->add(['meter_type_id' => $meter_type_id]);

		$data = request()->all();

		$client = request()->user()->userable;
		$usage_rates = array_pull($data, 'usageRates');

		// Create water rate
		$rate = new Rate();
		$rate->fill($data);
		$rate->client()->associate($client);
		$rate->save();

		$route = $rate->meterType->route . '.index';

		// Create water usage rates
		if ($usage_rates && count($usage_rates) > 0) {
			foreach ($usage_rates as $data) {

				$usage_rate = new UsageRate();
				$usage_rate->fill($data);
				$usage_rate->rate()->associate($rate);
				$usage_rate->save();
			}
		}

		flash('Rate Created')->success();

		return redirect()->route($route);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Rate $rate
	 * @return Response
	 * @internal param int $id
	 */
	public function edit(Rate $rate)
	{

		$form = $this->form(RateForm::class, [
			'method' => 'PUT',
			'model' => $rate,
			'url' => route('rates.update', ['id' => $rate->id]),
		]);
		return view('rates.edit', compact('form'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Rate $rate
	 * @return Response
	 * @internal param int $id
	 */
	public function update(Rate $rate)
	{

		$data = request()->all();

		$form = $this->form(RateForm::class);
		$form->redirectIfNotValid();

		$usage_rates = array_pull($data, 'usageRates');

		$rate->update(request(['code', 'name']));

		// assign custom charge to Rate
		$rate->customCharges()->sync(request()->input('customCharges'));


		// reset all TaxCodes for this rate, attach if have any
		$rate->taxCodes()->detach();

		//assign charge tax code and pbc tax code to rate
		if(!is_null(request()->input('usageTaxCodes'))){
			// $rate->taxCodes()->attach([request()->input('usageTaxCodes') => ['usage' => true]]);
			$rate->usageTaxCodes()->attach([request()->input('usageTaxCodes') => ['usage' => true]]);
		}

		if (!is_null(request()->input('pbcTaxCodes'))) {
			// $rate->taxCodes()->attach([request()->input('pbcTaxCodes') => ['pbc' => true]]);
			$rate->pbcTaxCodes()->attach([request()->input('pbcTaxCodes') => ['pbc' => true]]);
		}

		// Create or update usage rates
		if ($usage_rates && count($usage_rates) > 0) {

			foreach ($usage_rates as $data) {

				$usage_rate = UsageRate::updateOrCreate(['id'=>$data['id']], $data);
				$usage_rate->rate()->associate($rate);
				$usage_rate->save();
			}
		}

		flash('Rate Updated')->success();

		$route = $rate->meterType->route . '.index';
		return redirect()->route($route);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$row = Rate::findOrFail($id);
		$route = $row->meterType->route . '.index';
		$row->delete();

		flash('Rate Deleted')->success();

		return redirect()->route($route);
	}

}

?>