<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Meter;
use Arcanedev\LaravelNotes\Models\Note;
use Auth;
use Illuminate\Http\Request;
use Validator;

class NoteController extends Controller
{

	public function CustomerIndex(Customer $customer)
	{

		$rows = $customer->notes;
		return view('notes.index', compact('rows', 'customer'));
	}

	public function update(Request $request, $id)
	{

		$rules = array(
			'content' => 'required',
		);
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {

			$note = Note::find($id);
			$note->update(request()->only(['content']));
			return response()->json(['success' => 'Note edited.']);
		}

		return response()->json(['error' => $validator->errors()->all()]);

	}


	public function updateTableContainer($module, $id)
	{

		$rows = [];

		switch($module){
			case 'customer':
				$rows = Customer::find($id)->notes;
				break;

			case 'meters':
				$rows = Meter::find($id)->notes;
		}

		if(empty($rows))
			$html = [];
		else
			$html = view('notes.table', compact('rows'))->render();

		return response()->json(compact('html'));
	}


	public function destroy(Note $note)
	{
		$note->delete();

		return redirect()->back();
    }
}
