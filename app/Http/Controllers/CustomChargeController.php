<?php

namespace App\Http\Controllers;

use App\CustomCharge;

class CustomChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = CustomCharge::all();

        return view('select.custom-charge.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('select.custom-charge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
	    $this->validate(request(), [
		    'code' => 'required|max:32',
		    'name' => 'required|max:191',
		    'value' => 'required|numeric|between:0,9999.99',
	    ]);

	    CustomCharge::create(request()->only('code', 'name', 'value', 'omit_if_zero_usage'));

	    flash('Custom Charge Added')->success();

	    return redirect()->route('custom-charge.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomCharge  $customCharge
     * @return \Illuminate\Http\Response
     */
    public function show(CustomCharge $customCharge)
    {

    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @return \Illuminate\Http\Response
	 * @internal param CustomCharge $customCharge
	 */
    public function edit($id)
    {
	    $entry = CustomCharge::findOrFail($id); //Get user with specified id

	    return view('select.custom-charge.edit', compact('entry')); //pass user and roles data to view
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $id
	 * @return \Illuminate\Http\Response
	 * @internal param \Illuminate\Http\Request $request
	 * @internal param CustomCharge $customCharge
	 */
    public function update($id)
    {
	    $row = CustomCharge::findOrFail($id); //Get role specified by id

	    //Validate code, name
	    $this->validate(request(), [
		    'code' => 'required|max:32',
		    'name' => 'required|max:191',
		    'value' => 'required|numeric|between:0,9999.99',

	    ]);
	    $input = request()->only(['name', 'code', 'value', 'omit_if_zero_usage']);
	    $row->fill($input)->save();

	    flash('Custom Charge Updated')->success();

	    return redirect()->route('custom-charge.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomCharge  $customCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $row = CustomCharge::findOrFail($id);
	    $row->delete();

	    flash('Custom Charge Deleted')->success();

	    return redirect()->route('custom-charge.index');
    }
}
