<?php

namespace App\Http\Controllers;

use App\Zone;

class ZoneController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		$rows = Zone::all();

		return view('select.zone.index', compact('rows'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('select.zone.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);

		Zone::create(request()->only('code', 'name'));

		flash('Zone Added')->success();

		return redirect()->route('zone.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id) {

		$entry = Zone::findOrFail($id); //Get user with specified id

		return view('select.zone.edit', compact('entry')); //pass user and roles data to view
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id) {

		$row = Zone::findOrFail($id); //Get role specified by id

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);
		$input = request()->only(['name', 'code']);
		$row->fill($input)->save();

		flash('Zone Updated')->success();

		return redirect()->route('zone.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id) {
		$row = Zone::findOrFail($id);
		$row->delete();

		flash('Zone Deleted')->success();

		return redirect()->route('zone.index');
	}

}

?>