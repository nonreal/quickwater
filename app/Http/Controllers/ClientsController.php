<?php

namespace App\Http\Controllers;

use App\Client;
use App\Forms\ClientForm;
use App\Forms\CreateClientForm;
use App\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class ClientsController extends Controller
{

	use FormBuilderTrait;

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
	    $clients = Client::all();

	    return view('clients.index', compact('clients'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param FormBuilder $formBuilder
	 * @return \Illuminate\Http\Response
	 */
	public function create(FormBuilder $formBuilder) {

		$form = $formBuilder->create(CreateClientForm::class, [
			'method' => 'POST',
			'url' => route('clients.store'),
		]);

		return view('clients.create', compact('form'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(\request()->all());

	    $form = $this->form(CreateClientForm::class);
	    $form->redirectIfNotValid();
	    $data = $form->getFieldValues(false);

	    // Create User
	    $user = User::create([
		    'name' => $data['client']['first_name'] . ' ' . $data['client']['first_name'],
		    'email' => $data['user']['email'],
		    'password' => $data['user']['password'],
	    ]);
	    $user->assignRole('client');

	    // Create Client
	    $client = new Client;
	    $client->fill($data['client']);
	    $client->save();

	    // Add Address
	    foreach ($data['client']['addresses'] as $address) {
		    if (!isset($address['delete_address']))
			    $client->addAddress($address);
	    }

	    // Add Client to userable
	    $client->user()->save($user);

	    flash('Client created.')->success();

	    return redirect()->route('clients.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Client $client
	 * @param FormBuilder $formBuilder
	 * @return \Illuminate\Http\Response
	 * @internal param $id
	 * @internal param Client $client
	 */
	public function edit(Client $client) {


		$form = $this->form(ClientForm::class, [
			'method' => 'PUT',
			'model' => $client,
			'url' => route('client.update', ['id' => $client->id]),
			// 'language_name' => 'translations',
		]);

		return view('clients.edit', compact('form'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param Client $client
	 * @return \Illuminate\Http\Response
	 * @internal param int $id
	 */
    public function update(Request $request, Client $client)
    {
        //
        // dd(\request()->all());

	    $form = $this->form(ClientForm::class);
	    $form->redirectIfNotValid();
	    $data = $form->getFieldValues(false);

	    // update Client
	    $client->update($data);

	    //remove all addresses
	    $client->flushAddresses();

	    foreach ($data['addresses'] as $address) {
		    if (!isset($address['delete_address']))
			    $client->addAddress($address);
	    }

	    flash('Client successfully edited.')->success();

	    return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
