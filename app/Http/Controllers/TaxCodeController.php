<?php

namespace App\Http\Controllers;

use App\TaxCode;

class TaxCodeController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		$rows = TaxCode::all();

		return view('select.tax-code.index', compact('rows'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('select.tax-code.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:10',
			'name' => 'required|max:191',
			'rate' => 'required|numeric',
		]);

		TaxCode::create(request()->only('code', 'name', 'rate'));

		flash('TaxCode Added')->success();

		return redirect()->route('tax-code.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id) {

		$entry = TaxCode::findOrFail($id); //Get user with specified id

		return view('select.tax-code.edit', compact('entry')); //pass user and roles data to view
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id) {

		$row = TaxCode::findOrFail($id); //Get role specified by id

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:10',
			'name' => 'required|max:191',
			'rate' => 'required|numeric',
		]);
		$input = request()->only(['name', 'code', 'rate']);
		$row->fill($input)->save();

		flash('TaxCode Updated')->success();

		return redirect()->route('tax-code.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id) {
		$row = TaxCode::findOrFail($id);
		$row->delete();

		flash('TaxCode Deleted')->success();

		return redirect()->route('tax-code.index');
	}

}

?>