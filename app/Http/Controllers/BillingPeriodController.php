<?php

namespace App\Http\Controllers;

use App\BillingPeriod;
use App\BillingPeriodUnit;
use Illuminate\Http\Request;

class BillingPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$rows = BillingPeriod::all();

    	return view('select.billing-period.index', compact('rows'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $range = BillingPeriodUnit::all()->pluck('display_name', 'id');
	    return view('select.billing-period.create', compact('range'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    //Validate code, name
	    $this->validate(request(), [
		    'code' => 'required|max:32',
		    'name' => 'required|max:191',
		    'date' => 'required|integer|between:1,31',
	    ]);

	    BillingPeriod::create(request()->only('code', 'name', 'date', 'billing_period_unit_id'));

	    flash('Billing Period Added')->success();

	    return redirect()->route('billing-period.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BillingPeriod  $billingPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(BillingPeriod $billingPeriod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BillingPeriod  $billingPeriod
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $entry = BillingPeriod::findOrFail($id); //Get user with specified id
	    $range = BillingPeriodUnit::all()->pluck('display_name', 'id');

	    return view('select.billing-period.edit', compact('entry', 'range')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BillingPeriod  $billingPeriod
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

    	// dump(\request()->all());

	    $row = BillingPeriod::findOrFail($id); //Get role specified by id

	    //Validate code, name
	    $this->validate(request(), [
		    'code' => 'required|max:32',
		    'name' => 'required|max:191',
		    'date' => 'required|integer|between:1,31',
	    ]);


	    $input = request()->only(['name', 'code', 'date', 'billing_period_unit_id']);
	    $row->fill($input)->save();

	    flash('Billing Period Updated')->success();

	    return redirect()->route('billing-period.index');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BillingPeriod  $billingPeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillingPeriod $billingPeriod)
    {
        //
    }
}
