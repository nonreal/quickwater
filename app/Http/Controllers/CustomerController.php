<?php

namespace App\Http\Controllers;

use App\Forms\CustomerForm;
use App\Forms\WaterMeterForm;
use App\User;
use App\Customer;
use App\Forms\CreateCustomerForm;
use Auth;
use DB;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Validator;

class CustomerController extends Controller
{

	use FormBuilderTrait;

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
	    $customers = Customer::with('meters')->get();

	    return view('customers.index', compact('customers'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param FormBuilder $formBuilder
	 * @return \Illuminate\Http\Response
	 */
	public function create(FormBuilder $formBuilder)
	{

		$form = $formBuilder->create(CreateCustomerForm::class, [
			'method' => 'POST',
			'url' => route('customers.store'),
		]);

		return view('customers.create', compact('form'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(request()->user()->toArray());


	    $form = $this->form(CreateCustomerForm::class);
	    $form->redirectIfNotValid();
	    $data = $form->getFieldValues(false);

	    $client = $request->user()->userable;


	    try {
		    DB::beginTransaction();


		    // Create user
		    $user = User::create([
			    'name' => $data['customer']['first_name'] . ' ' . $data['customer']['last_name'],
			    'email' => $data['user']['email'],
			    'password' => bcrypt($data['user']['password']),
		    ]);

		    $user->assignRole('customer');

		    // Create Customer
		    $customer = new Customer;
		    $customer->fill($data['customer']);
		    $customer->client()->associate($client);
		    $customer->save();

		    // Add customer to userable
		    $customer->user()->save($user);

		    $customer->addUpdateAddresses($data['customer']['addresses']);

		    DB::commit();

		    flash('Customer created.')->success();

	    } catch (\Exception $e) {
		    DB::rollback();

		    dd($e->getMessage());

		    flash('Something went wrong, try again.')->error();

		    back();

	    }

	    return redirect()->route('customers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {

	    $customer->meters;

	    /*
	    foreach($customer->meters as $meter){
	    	dump($meter->toArray());
	    	dump($meter->readings->last());
	    }
	    dd('.');
	    */


	    $menu = \Menu::get('Sidebar')->get('customers');
	    $menu->add('Notes', ['route' => array('customers-notes.index', 'customer'=>$customer)]);
	    // $menu->add('orders', array('action' => array('NoteController@CustomerIndex', 'customer' => $customer)));


    	$form = $this->form(WaterMeterForm::class, [
		    'method' => 'POST',
		    'url' => route('meters.create', ['customer' => $customer]),
	    ]);

	    return view('customers.show', compact('customer', 'form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
	    $form = $this->form(CustomerForm::class, [
		    'method' => 'PUT',
		    'model' => $customer,
		    'url' => route('customers.update', ['id' => $customer->id]),
		    'data' => ['id' => $customer->id],
		    // 'language_name' => 'translations',
	    ]);

	    return view('customers.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {

    	// dd(request()->all());

	    // $form = $this->form(CustomerForm::class);
	    $form = $this->form(CustomerForm::class, [
		    'model' => $customer,
		    'data' => ['id' => $customer->id],
	    ]);

	    $form->redirectIfNotValid();
	    $data = $form->getFieldValues(false);

	    // update Model
	    $customer->update($data);

	    //remove all addresses
	    $customer->flushAddresses();

	    foreach ($data['addresses'] as $address) {
		    if (!isset($address['delete_address']))
			    $customer->addAddress($address);
	    }

	    flash('Customer successfully edited.')->success();

	    return redirect()->route('customers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //

	    // remove addresses
	    $addresses = $customer->addresses;
	    foreach($addresses as $address)
		    $customer->deleteAddress($address);

	    // remove meters

	    //remove user
	    $customer->user->delete();

	    // remove customer
	    $customer->delete();


	    flash('Customer Deleted')->success();

	    return redirect()->route('customers.index');

    }


	public function addNote(Request $request, Customer $customer)
	{
		$rules = array(
			'content' => 'required',
		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			flash('Note is required')->error();

			return redirect()->back();
		}

		$customer->createNote($request->input('content'), Auth::user());

		flash('Note added.')->success();

		return redirect()->back();

    }

}
