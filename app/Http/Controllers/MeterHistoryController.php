<?php

namespace App\Http\Controllers;

use App\Meter;
use App\MeterHistory;
use Illuminate\Http\Request;

class MeterHistoryController extends Controller
{

	public $actionAssign = 'assign';
	public $actionRemove = 'remove';


	public function log($meter_id, $customer_id, $action)
	{

		$meterLog = new MeterHistory();
		$meterLog->action = $action;
		$meterLog->meter_id = $meter_id;
		$meterLog->customer_id = $customer_id;

		$meterLog->save();
	}

}
