<?php

namespace App\Http\Controllers;

use App\MeterType;
use Request;

class MeterTypeController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$rows = MeterType::all();

		return view('select.meter-type.index', compact('rows'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('select.meter-type.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);

		MeterType::create(request()->only('code', 'name'));

		flash('Meter Type Added')->success();
		return redirect()->route('meter-type.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id) {

		$entry = MeterType::findOrFail($id); //Get user with specified id

		return view('select.meter-type.edit', compact( 'entry')); //pass user and roles data to view
	}

	public function update($id) {

		$row = MeterType::findOrFail($id); //Get role specified by id

		//Validate code, name
		$this->validate(request(), [
			'code' => 'required|max:4',
			'name' => 'required|max:191',
		]);
		$input = request()->only(['name', 'code']);
		$row->fill($input)->save();

		flash('Meter Type Updated')->success();

		return redirect()->route('meter-type.index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id) {

		$row = MeterType::findOrFail($id);
		$row->delete();

		flash('Meter Type Deleted')->success();

		return redirect()->route('meter-type.index');

	}

}

?>