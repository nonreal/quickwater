<?php

namespace App\Http\Controllers;

use App\MeterType;
use App\Rate;
use App\Forms\RateForm;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;


class WaterRateController extends RateController
{

	use FormBuilderTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rows = Rate::all();

		return view('rates.water.index', compact('rows'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param FormBuilder $formBuilder
	 * @return Response
	 */

	public function create(FormBuilder $formBuilder)
	{

		$form = $formBuilder->create(RateForm::class, [
			'method' => 'POST',
			'url' => route('water-rates.store'),
		]);

		return view('rates.water.create', compact('form'));
	}

	/**
	 * Store a newly created resource in storage.
	 * @param null $meter_type_id
	 * @return Response
	 */
	public function store( $meter_type_id = null )
	{

		$meter_type_id = MeterType::where('code', 'W')->pluck('id')->first();

		if($meter_type_id)
			request()->request->add(['meter_type_id' => $meter_type_id]);

		$form = $this->form(RateForm::class);
		$form->redirectIfNotValid();

		// dd(request()->toArray());

		return parent::store($meter_type_id);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{

	}

}

?>