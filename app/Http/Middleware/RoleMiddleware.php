<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission)
    {

	    if (Auth::guest()) {
		    return redirect('/');
	    }

	    if (!$request->user()->hasRole($role)) {
		    // return redirect('/')->with('403', 'Unauthorized Access');
		    return response()->view('errors.403', array(), 401);
	    }

	    if (!$request->user()->hasPermissionTo($permission)) {
		    // return redirect('/')->with('403', 'Unauthorized Access');
		    return response()->view('errors.403', array(), 401);
	    }

    	return $next($request);
    }
}
