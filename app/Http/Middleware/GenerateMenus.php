<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


	    \Menu::make('Sidebar', function ($menu) {

		    $menu->add('Home', ['route' => 'homepage', 'class' => 'navbar navbar-home'])->prepend('<i class="fa fa-home"></i>');

		    $menu->add('Clients', ['route' => 'clients.index'])->prepend('<i class="fa fa-user"></i>')
			    ->data('permission', 'list-clients');

		    $menu->add('Customers', ['route' => 'customers.index'])->prepend('<i class="fa fa-user"></i>')
			    ->data('permission', 'list-customers');

		    $menu->add('Readings', ['route'=> 'readings.index'])->prepend('<i class="fa fa-clock-o" aria-hidden="true"></i>')
			    ->data('permission', 'list-readings');

		    $menu->add('Meters', ['route' => 'meters.index'])->prepend('<i class="fa fa-clock-o" aria-hidden="true"></i>')
			    ->data('permission', 'list-meters');

		    $menu->add('Water Rates', ['route' => 'water-rates.index'])->prepend('<i class="fa fa-bar-chart" aria-hidden="true"></i>')
			    ->data('permission', 'list-rates');

		    // Configuration items
		    $menu->add('Configuration', ['class' => 'treeview'])->prepend('<i class="fa fa-gear" aria-hidden="true"></i>');
		    $menu->configuration->add('Meter Types', ['route'=>'meter-type.index']);
		    $menu->configuration->add('Zones', ['route'=>'zone.index']);
		    $menu->configuration->add('Tax Codes', ['route'=>'tax-code.index']);
		    $menu->configuration->add('Billing Periods', ['route'=>'billing-period.index']);
		    $menu->configuration->add('Custom Charges', ['route'=>'custom-charge.index']);

		    // User roles
		    $menu->add('Roles Permissions', ['class' => 'treeview'])->prepend('<i class="fa fa-key" aria-hidden="true"></i>');
		    $menu->rolesPermissions->add('Users', ['route' => 'users.index'])->data('permission', 'edit-roles-permissions');
		    $menu->rolesPermissions->add('Roles', ['route' => 'roles.index'])->data('permission', 'edit-roles-permissions');
		    $menu->rolesPermissions->add('Permissions', ['route' => 'permissions.index'])->data('permission', 'edit-roles-permissions');



	    })->filter(function ($item) {

	    	if($item->data('permission')){
			    if (Auth::user() && Auth::user()->can($item->data('permission'))) {
				    return true;
			    }
			    return false;
		    }

		    return true;

	    });


    	return $next($request);


	    /*
	     *
Menu::adminlteSubmenu('Roles & Permissions', 'key')
->routeIfCan('edit-roles-permissions', 'users.index', ' <i class="fa fa-user" aria-hidden="true"></i>Users')
->routeIfCan('edit-roles-permissions', 'roles.index', ' <i class="fa fa-key" aria-hidden="true"></i>Roles')
->routeIfCan('edit-roles-permissions', 'permissions.index', ' <i class="fa fa-key" aria-hidden="true"></i>Permissions')

	    <ul data-widget="tree" class="sidebar-menu tree">
		    <li class="header">HEADER</li>
		    <li><a href="http://quickwater.test"><i class="fa fa-home"></i><span>Home</span></a></li>
		    <li><a href="http://quickwater.test/clients"><i aria-hidden="true" class="fa fa-user"></i><span>Clients</span></a></li>
		    <li class="active"><a href="http://quickwater.test/customers"><i aria-hidden="true" class="fa fa-user"></i><span>Customers</span></a></li>
		    <li><a href="http://quickwater.test/meters"><i aria-hidden="true" class="fa fa-clock-o"></i><span>Meters</span></a></li>
		    <li><a href="http://quickwater.test/water-rates"><i aria-hidden="true" class="fa fa-bar-chart"></i><span>Water Rates</span></a></li>
		    <li class="treeview"><a href="#"><i class="fa fa-gear"></i><span> Configuration</span> <i class="fa fa-angle-left pull-right"></i></a>
		        <ul class="treeview-menu">
		            <li><a href="http://quickwater.test/meter-type">Meter Types</a></li>
		            <li><a href="http://quickwater.test/route">Routes</a></li>
		            <li><a href="http://quickwater.test/zone">Zones</a></li>
		            <li><a href="http://quickwater.test/tax-code">Tax Codes</a></li>
		            <li><a href="http://quickwater.test/billing-period">Billing Periods</a></li>
		            <li><a href="http://quickwater.test/custom-charge">Custom Charges</a></li>
		        </ul>
		    </li>
		    <li class="treeview"><a href="#"><i class="fa fa-key"></i><span> Roles &amp; Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
		        <ul class="treeview-menu">
		            <li><a href="http://quickwater.test/users"><i aria-hidden="true" class="fa fa-user"></i>Users</a></li>
		            <li><a href="http://quickwater.test/roles"><i aria-hidden="true" class="fa fa-key"></i>Roles</a></li>
		            <li><a href="http://quickwater.test/permissions"><i aria-hidden="true" class="fa fa-key"></i>Permissions</a></li>
		        </ul>
		    </li>
		</ul>
	    */



    }
}
