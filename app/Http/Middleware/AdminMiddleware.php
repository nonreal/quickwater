<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

	    if (!Auth::user()->hasPermissionTo('edit-roles-permissions')) //If user does //not have this permission
	    {
		    // abort(403, 'Unauthorized action.');

		    return response()->view('errors.403', array(), 401);
	    }

        return $next($request);
    }
}
