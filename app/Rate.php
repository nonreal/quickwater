<?php

namespace App;

class Rate extends Model
{

    protected $table = 'rates';

    protected $with = ['client', 'meterType', 'usageRates', 'customCharges', 'taxCodes'];

    public $timestamps = true;


	public function meter()
	{
		return $this->hasMany(Meter::class);
	}


	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function meterType()
	{
		return $this->belongsTo(MeterType::class);
	}

    public function usageRates()
    {
        return $this->hasMany(UsageRate::class);
    }


	public function customCharges()
	{
		return $this->belongsToMany(CustomCharge::class)->withTimestamps();
    }

	public function taxCodes()
	{
		return $this->belongsToMany(TaxCode::class)->withPivot('usage', 'pbc');
	}


	public function usageTaxCodes()
	{
		return $this->belongsToMany(TaxCode::class)->where('usage', 1);
	}

	public function pbcTaxCodes()
	{
		return $this->belongsToMany(TaxCode::class)->where('pbc', 1);
	}


}