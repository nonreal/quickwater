<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\NoteController;

Auth::routes();

Route::get('/about', function () {
	return view('about');
});

Route::get('/', 'HomeController@index')->name('homepage');

Route::group(['middleware' => 'auth'], function () {

	Route::resource('users', 'UserController');
	Route::resource('roles', 'RoleController');
	Route::resource('permissions', 'PermissionController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes

	Route::middleware(['role:admin,list-clients,edit-clients'])->group(function(){

		// Route::get('/clients', 'ClientsController@index')->name('clients.index');
		// Route::post('/clients', 'ClientsController@store')->name('clients.store');
		// Route::get('/clients/create', 'ClientsController@create')->name('client.create');
		// Route::get('/clients/{client}/edit', 'ClientsController@edit')->name('client.edit');
		// Route::put('/clients/{client}', 'ClientsController@update')->name('client.update');

		Route::resource('/clients', 'ClientsController');

	});



	Route::middleware(['role:client,list-rates,list-customers,list-meters'])->group(function () {


		Route::resource('/meter-type', 'MeterTypeController');
		Route::resource('/route', 'RouteController');
		Route::resource('/zone', 'ZoneController');
		Route::resource('/tax-code', 'TaxCodeController');
		Route::resource('/billing-period', 'BillingPeriodController');
		Route::resource('/custom-charge', 'CustomChargeController');


		Route::resource('rates', 'RateController');
		Route::get('/rates/{rate}/edit', 'RateController@edit')->name('rates.edit');

		// Route::get('/water-rates', 'WaterRateController@index')->name('water-rates.index');
		// Route::post('/water-rates', 'WaterRateController@store')->name('water-rates.store');
		// Route::get('/water-rates/create', 'WaterRateController@create')->name('water-rates.create');
		// Route::get('/water-rates/{id}', 'WaterRateController@show')->name('water-rates.show');
		// Route::get('/water-rates/{rate}/edit', 'WaterRateController@edit')->name('water-rates.edit');
		// Route::put('/water-rates/{rate}', 'WaterRateController@update')->name('water-rates.update');

		Route::resource('/water-rates', 'WaterRateController');


		// Route::get('/customers', 'CustomerController@index')->name('customers.index');
		// Route::post('/customers', 'CustomerController@store')->name('customers.store');
		// Route::get('/customers/create', 'CustomerController@create')->name('customers.create');
		// Route::get('/customers/{customer}', 'CustomerController@show')->name('customers.show');
		// Route::get('/customers/{customer}/edit', 'CustomerController@edit')->name('customers.edit');
		// Route::put('/customers/{customer}', 'CustomerController@update')->name('customers.update');
		// Route::delete('/customers/{customer}', 'CustomerController@destroy')->name('customers.destroy');

		Route::resource('/customers', 'CustomerController');


		Route::get('/notes/', 'NoteController@index')->name('notes');
		Route::put('/notes/{note}', 'NoteController@update')->name('notes.update');
		Route::delete('notes/{note}', 'NoteController@destroy')->name('notes.destroy');
		Route::get('/notes/updateTable/{module}/{id}', 'NoteController@updateTableContainer')->name('notes.update-table');

		Route::get('/customers/{customer}/notes', 'NoteController@CustomerIndex')->name('customers-notes.index');
		Route::post('/customers/{customer}/note', 'CustomerController@addNote')->name('customers-note.add');

		Route::post('/meters/{meter}/note', 'MeterController@addNote')->name('meters-note.add');



		Route::get('/meters', 'MeterController@index')->name('meters.index');
		Route::get('/meters/water/free', 'MeterController@getUnasigned')->name('meters.unasigned');
		Route::post('/meters/assign', 'MeterController@addCustomer')->name('meters.addCustomer');

		Route::post('/customers/{customer}/meters', 'MeterController@store')->name('meters.create');
		Route::post('/meters/{meter}/removeCustomer', 'MeterController@removeCustomer')->name('meters.remove-customer');
		Route::post('/meters/{meter}/setOutofOrder', 'MeterController@setOutofOrder')->name('meters.outoforder');
		Route::get('/meters/{meter}/edit', 'MeterController@edit')->name('meters.edit');
		Route::put('/meters/{meter}', 'MeterController@update')->name('meters.update');
		Route::delete('/meters/{meter}', 'CustomerController@destroy')->name('meters.destroy');


		Route::get('/readings', 'ReadingController@index')->name('readings.index');

	});






});
