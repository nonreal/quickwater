<?php

Breadcrumbs::register('home', function($breadcrumbs)
{
	$breadcrumbs->push('Home', route('home'));
});


//CLIENTS

Breadcrumbs::register('clients', function($breadcrumbs)
{
	$breadcrumbs->push('Clients', route('clients.index'));
});

Breadcrumbs::register('clients.create', function($breadcrumbs)
{
	$breadcrumbs->parent('clients');
	$breadcrumbs->push('Create');
});

Breadcrumbs::register('clients.edit', function($breadcrumbs)
{
	$breadcrumbs->parent('clients');
	$breadcrumbs->push('Edit');
});


//CUSTOMERS

Breadcrumbs::register('customers', function($breadcrumbs)
{
	$breadcrumbs->push('Customers', route('customers.index'));
});

Breadcrumbs::register('customers.view', function ($breadcrumbs, $customer) {
	$breadcrumbs->parent('customers');
	$breadcrumbs->push($customer->full_name, route('customers.view', $customer->id));
});

Breadcrumbs::register('customers.create', function($breadcrumbs)
{
	$breadcrumbs->parent('customers');
	$breadcrumbs->push('Create');
});

Breadcrumbs::register('customers.edit', function($breadcrumbs, $customer)
{
	$breadcrumbs->parent('customers');
	$breadcrumbs->push($customer->full_name, route('customers.view', $customer->id));
	$breadcrumbs->push('Edit');
});

// WATER RATES
Breadcrumbs::register('water-rates', function ($breadcrumbs) {
	$breadcrumbs->push('Water Rates', route('water-rates.index'));
});


Breadcrumbs::register('water-rates.create', function ($breadcrumbs) {
	$breadcrumbs->parent('water-rates');
	$breadcrumbs->push('Create');
});

Breadcrumbs::register('water-rates.view', function ($breadcrumbs, $waterRate) {
	$breadcrumbs->parent('water-rates');
	$breadcrumbs->push($waterRate->name, route('water-rates.view', $waterRate->id));
});

Breadcrumbs::register('water-rates.edit', function ($breadcrumbs, $waterRate) {
	$breadcrumbs->parent('water-rates');
	$breadcrumbs->push($waterRate->name, route('water-rates.view', $waterRate->id));
	$breadcrumbs->push('Edit');
});